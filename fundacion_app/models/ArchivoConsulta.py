# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from fundacion_app.models import Consulta

class ArchivoConsulta(models.Model):
    consulta = models.ForeignKey(Consulta, related_name="archivos")
    archivo = models.FileField(upload_to="archivos_consultas/", null=True, blank=True)

    eliminado = models.BooleanField(default=False)
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    fecha_modificacion = models.DateTimeField(auto_now=True)

    class Meta:
        app_label = 'fundacion_app'
