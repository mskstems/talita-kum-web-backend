# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from fundacion_app.models import Paciente, Perfil

class CobrosPaciente(models.Model):
    "Cobros de pacientes"

    COBRO = 10
    ABONO = 20

    TIPOS = (
        (COBRO, "COBRO"),
        (ABONO, "ABONO")
    )

    paciente = models.ForeignKey(Paciente, related_name="cobros")
    colaborador = models.ForeignKey(Perfil, related_name="cobros_paciente")

    monto = models.FloatField(null=False, blank=False)
    motivo = models.TextField(null=True, blank=True)
    pagado = models.BooleanField(default=False)
    anulado = models.BooleanField(default=False)
    tipo = models.SmallIntegerField(choices=TIPOS, null=True, blank=True)

    fecha_creacion = models.DateTimeField(auto_now_add=True)
    fecha_modificacion = models.DateTimeField(auto_now=True)

    class Meta:
        app_label = 'fundacion_app'
