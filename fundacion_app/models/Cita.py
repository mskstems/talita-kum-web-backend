# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from fundacion_app.models import Perfil, Paciente

class Cita(models.Model):
    colaborador = models.ForeignKey(Perfil, related_name="citas")
    paciente = models.ForeignKey(Paciente, related_name="citas")
    anulada = models.BooleanField(default=False)
    descripcion = models.TextField(default="", null=True, blank=True)
    fecha_cita = models.DateTimeField(default=None, null=False, blank=False)
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    fecha_modificacion = models.DateTimeField(auto_now=True)

    class Meta:
        app_label = 'fundacion_app'
