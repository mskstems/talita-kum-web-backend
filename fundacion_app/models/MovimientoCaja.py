# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from fundacion_app.models import AperturaCierreCaja

class MovimientoCaja(models.Model):
    INGRESO = 10
    EGRESO = 20
    TIPOS = (
        (INGRESO, "INGRESO"),
        (EGRESO, "EGRESO")
    )

    RECIBO = 10
    FACTURA = 20
    TIPOS_DOC = (
        (RECIBO, "RECIBO"),
        (FACTURA, "FACTURA"),
    )

    apertura_cierre_caja = models.ForeignKey(AperturaCierreCaja, related_name="movimientos")

    concepto = models.TextField(blank=True, null=True, default="")
    cantidad = models.FloatField(null=True, blank=True)
    tipo = models.CharField(max_length=2, blank=False, null=False, choices=TIPOS, default=None)

    tipo_documento = models.CharField(max_length=2, blank=True, null=True, choices=TIPOS_DOC)
    num_documento = models.CharField(max_length=64, blank=True, null=True)

    fecha_creacion = models.DateTimeField(auto_now_add=True)
    fecha_modificacion = models.DateTimeField(auto_now=True)

    class Meta:
        app_label = 'fundacion_app'
