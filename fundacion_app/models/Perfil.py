# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from fundacion_app.models import Persona
from fundacion_app.models import Profesion
from django.contrib.auth.models import User
from multiselectfield import MultiSelectField

class Perfil(models.Model):
    INVITADO = '10'
    SECRETARIA = '20'
    PSICOLOGO = '30'
    MEDICO = '40'
    ADMIN = '100'

    ROLES = (
        (INVITADO, 'INVITADO'),
        (SECRETARIA, 'SECRETARÍA'),
        (PSICOLOGO, 'PSICOLOGÍA'),
        (MEDICO, 'MEDICINA'),
        (ADMIN, 'ADMINISTRACIÓN'),
    )

    user = models.OneToOneField(User, related_name="perfil")
    persona = models.OneToOneField(Persona, related_name="perfil")
    profesion = models.ForeignKey(Profesion, related_name="perfiles", null=True, blank=True)
    roles = MultiSelectField(choices=ROLES, default=[])
    colegiado = models.CharField(max_length=50, null=True, blank=True)
    estado = models.BooleanField(default=True)
    eliminado = models.BooleanField(default=False)
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    fecha_modificacion = models.DateTimeField(auto_now=True)

    class Meta:
        app_label = 'fundacion_app'
