# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models

class Profesion(models.Model):
    profesion = models.CharField(max_length=50, null=False, blank=False)
    eliminado = models.BooleanField(default=False)
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    fecha_modificacion = models.DateTimeField(auto_now=True)

    class Meta:
        app_label = 'fundacion_app'

    def __unicode__(self):
        return self.profesion
