# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from fundacion_app.models import Paciente, Perfil

class Consulta(models.Model):

    paciente = models.ForeignKey(Paciente, related_name="consultas")
    colaborador = models.ForeignKey(Perfil, related_name="consultas")

    motivo = models.TextField(null=True, blank=True)

    ##  JSON'S CONVERTIDO A STRING
    # historia = models.TextField(null=True, blank=True)
    notas = models.TextField(null=True, blank=True)

    eliminado = models.BooleanField(default=False)
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    fecha_modificacion = models.DateTimeField(auto_now=True)

    class Meta:
        app_label = 'fundacion_app'
