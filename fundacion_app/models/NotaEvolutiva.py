# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from fundacion_app.models import Caso

class NotaEvolutiva(models.Model):

    caso = models.ForeignKey(Caso, related_name="notas_evolutivas")

    num_sesion = models.IntegerField(default=1, null=True, blank=True)
    vario = models.TextField(null=True, blank=True)
    tarea_anterior = models.TextField(null=True, blank=True)
    avance_terapeutico = models.TextField(null=True, blank=True)
    eventos_relevantes = models.TextField(null=True, blank=True)
    comentario_paciente = models.TextField(null=True, blank=True)
    tarea_proxima = models.TextField(null=True, blank=True)
    observaciones = models.TextField(null=True, blank=True)
    notas = models.TextField(null=True, blank=True)
    evolucion_psicologica = models.TextField(null=True, blank=True)
    evolucion_fisica = models.TextField(null=True, blank=True)

    anulada = models.BooleanField(default=False)
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    fecha_modificacion = models.DateTimeField(auto_now=True)

    class Meta:
        app_label = 'fundacion_app'
