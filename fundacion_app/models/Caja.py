# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models

class Caja(models.Model):

    APERTURADA = 10
    CERRADA = 20
    ESTADOS = (
        (APERTURADA, "APERTURADA"),
        (CERRADA, "CERRADA")
    )

    # saldo_actual = models.FloatField(default=0, null=True, blank=True)
    # ultimo_cierre = models.DateTimeField(blank=True, null=True)
    # estado = models.SmallIntegerField(choices=ESTADOS, default=CERRADA)

    fecha_creacion = models.DateTimeField(auto_now_add=True)
    fecha_modificacion = models.DateTimeField(auto_now=True)

    class Meta:
        app_label = 'fundacion_app'
