# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from fundacion_app.models import Caja, Perfil

class AperturaCierreCaja(models.Model):

    colaborador = models.ForeignKey(Perfil, related_name="aperturas_cierres_caja", blank=True, null=True)
    caja = models.ForeignKey(Caja, related_name="aperturas_cierres_caja", blank=True, null=True)

    estado = models.SmallIntegerField(choices=Caja.ESTADOS, default=Caja.APERTURADA)
    total_apertura = models.FloatField(null=True, blank=True)
    total_actual = models.FloatField(null=True, blank=True)
    total_cierre = models.FloatField(null=True, blank=True)
    fecha_cierre = models.DateTimeField(blank=True, null=True)
    notas = models.TextField(null=True, blank=True)

    fecha_creacion = models.DateTimeField(auto_now_add=True)
    fecha_modificacion = models.DateTimeField(auto_now=True)

    # denom_apert: {
    #     type: 'JSON'
    # },
    # denom_cierre: {
    #     type: 'JSON'
    # },

    class Meta:
        app_label = 'fundacion_app'
