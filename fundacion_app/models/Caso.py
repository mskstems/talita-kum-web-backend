# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from fundacion_app.models import Paciente, Perfil

class Caso(models.Model):

    paciente = models.ForeignKey(Paciente, related_name="casos")
    colaborador = models.ForeignKey(Perfil, related_name="casos")

    motivo = models.TextField(null=True, blank=True)
    cerrado = models.BooleanField(default=False)

    ##  JSON'S CONVERTIDO A STRING
    notas = models.TextField(null=True, blank=True)
    genograma = models.ImageField(upload_to="genogramas/", null=True, blank=True)

    diagnostico = models.TextField(null=True, blank=True)
    plan_terapeutico = models.TextField(null=True, blank=True)
    instrumentos = models.TextField(null=True, blank=True)
    resultados = models.TextField(null=True, blank=True)
    pronostico = models.TextField(null=True, blank=True)
    observaciones = models.TextField(null=True, blank=True)
    fecha_cierre = models.DateTimeField(null=True, blank=True)

    eliminado = models.BooleanField(default=False)
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    fecha_modificacion = models.DateTimeField(auto_now=True)

    class Meta:
        app_label = 'fundacion_app'
