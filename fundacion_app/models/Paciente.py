# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from fundacion_app.models import Persona
from multiselectfield import MultiSelectField

class Paciente(models.Model):
    PSICOLOGIA = '10'
    MEDICINA = '20'

    TIPOS = (
        (PSICOLOGIA, 'PSICOLOGÍA'),
        (MEDICINA, 'MEDICINA'),
    )

    persona = models.OneToOneField(Persona, related_name="paciente")
    tipos = MultiSelectField(choices=TIPOS, default=[], null=True, blank=True)
    contacto_referencia = models.CharField(max_length=64, null=True, blank=True)
    telefono_referencia = models.CharField(max_length=32, null=True, blank=True)
    religion = models.CharField(max_length=32, null=True, blank=True)
    tipo_de_sangre = models.CharField(max_length=16, null=True, blank=True)
    num_hermanos = models.SmallIntegerField(default=0, null=True, blank=True)
    genero = models.CharField(max_length=16, null=True, blank=True)
    nivel_academico = models.CharField(max_length=32, null=True, blank=True)
    ocupacion = models.CharField(max_length=64, null=True, blank=True)
    lugar_nacimiento = models.TextField(null=True, blank=True)
    fecha_nacimiento = models.DateField(null=True, blank=True)
    eliminado = models.BooleanField(default=False)
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    fecha_modificacion = models.DateTimeField(auto_now=True)

    ##  MONTO DE CREDITO O DEBITO DEL PACIENTE EN LA FUNDACIÓN
    saldo = models.FloatField(null=True, blank=True, default=0)

    nombres_padres = models.TextField(null=True, blank=True)
    ocupacion_padres = models.TextField(null=True, blank=True)
    info_padres = models.TextField(null=True, blank=True)

    notas_psicologicas = models.TextField(null=True, blank=True)
    notas_medicas = models.TextField(null=True, blank=True)

    class Meta:
        app_label = 'fundacion_app'
