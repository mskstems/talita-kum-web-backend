# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models

class Persona(models.Model):
    persona = models.CharField(max_length=64, null=True, blank=True)
    apellido_casada = models.CharField(max_length=32, null=True, blank=True)
    cui = models.CharField(max_length=32, null=True, blank=True)
    telefono = models.CharField(max_length=32, null=True, blank=True)
    celular = models.CharField(max_length=32, null=True, blank=True)
    direccion = models.TextField(null=True, blank=True)
    nacionalidad = models.CharField(max_length=64, null=True, blank=True)
    departamento = models.CharField(max_length=64, null=True, blank=True)
    correo = models.CharField(max_length=64, null=True, blank=True)
    foto = models.ImageField(upload_to="fotos/", null=True, blank=True)
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    fecha_modificacion = models.DateTimeField(auto_now=True)

    class Meta:
        app_label = 'fundacion_app'
        #unique_together = ('field1', 'field2')

    def __unicode__(self):
        return self.persona
