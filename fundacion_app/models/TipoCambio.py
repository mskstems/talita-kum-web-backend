# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models

class TipoCambio(models.Model):
    usd_to_gtq = models.FloatField(null=False, blank=False)
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    fecha_modificacion = models.DateTimeField(auto_now=True)

    class Meta:
        app_label = 'fundacion_app'

    def __unicode__(self):
        return self.usd_to_gtq
