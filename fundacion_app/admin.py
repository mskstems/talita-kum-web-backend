# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib import admin

from .models import (
    Persona,
    Perfil,
    Profesion,
    TipoCambio
)

# Register your models here.

admin.site.register(Persona)
admin.site.register(Perfil)
admin.site.register(TipoCambio)
admin.site.register(Profesion)
