# -*- coding: utf-8 -*-
from django.dispatch import receiver
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from fundacion_app.models import Perfil, Persona

##  PERFIL SIGNALS
@receiver(post_save, sender=User)
def save_user(sender, instance, created, **kwargs):
    if created:
        ##  SI SE CREA UNA NUEVA INSTANCIA DEL MODELO USER, PERSONA, SE GUARDA Y SE RELACIONAN CON EL PERFIL
        persona = Persona.objects.create(persona=instance.first_name, correo=instance.email)
        perfil = Perfil.objects.create(user=instance, persona=persona)
        if instance.is_superuser:
            perfil.roles.append(str(Perfil.ADMIN))
            perfil.save()

