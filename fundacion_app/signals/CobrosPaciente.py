# -*- coding: utf-8 -*-
from django.dispatch import receiver
from django.db.models.signals import post_save
from fundacion_app.models import CobrosPaciente, Paciente

##  COBROS PACIENTE SIGNALS

##  MOVIMIENTO DE CAJA SIGNALS
@receiver(post_save, sender=CobrosPaciente)
def cobro_paciente(sender, instance, created, **kwargs):
    ##  ACTUALIZA EL MONTO ACTUAL DE LA CREDITO O DEBITO DEL PACIENTE CON LA FUNDACIÓN
    try:
        # if created:
        #     cobro = CobrosPaciente.objects.get(pk=instance.id)
        #     paciente = cobro.paciente
        #     paciente.saldo += float(cobro.monto if cobro.monto else 0)
        #     paciente.save()

        # else:
        #     cobro = CobrosPaciente.objects.get(pk=instance.id)
        #     paciente = cobro.paciente

        #     if cobro.anulado or cobro.pagado:
        #         paciente.saldo -= float(cobro.monto if cobro.monto else 0)
        #         paciente.save()

        cobro = CobrosPaciente.objects.get(pk=instance.id)
        paciente = cobro.paciente

        if created:
            if cobro.tipo == CobrosPaciente.COBRO:
                ##  SI LA TRANSACCIÓN ES TIPO COBRO, SE RESTARÁ LA CANTIDAD DE LA CUENTA ACTUAL DEL PACIENTE
                # paciente.saldo -= float(cobro.monto if cobro.monto else 0)
                # paciente.save()
                pass

            elif cobro.tipo == CobrosPaciente.ABONO:
                ##  SI LA TRANSACCIÓN ES TIPO ABONO, SE SUMARÁ LA CANTIDAD DE LA CUENTA ACTUAL DEL PACIENTE
                paciente.saldo += float(cobro.monto if cobro.monto else 0)
                paciente.save()

        else:
            if cobro.pagado:
                if cobro.tipo == CobrosPaciente.COBRO:
                    ##  SI LA TRANSACCIÓN ES TIPO COBRO, SE SUMARÁ LA CANTIDAD PORQUE ES UNA ANULACIÓN
                    paciente.saldo -= float(cobro.monto if cobro.monto else 0)
                    paciente.save()

                # elif cobro.tipo == CobrosPaciente.ABONO:
                #     ##  SI LA TRANSACCIÓN ES TIPO ABONO, SE RESTARÁ LA CANTIDAD PORQUE ES UNA ANULACIÓN
                #     # paciente.saldo -= float(cobro.monto if cobro.monto else 0)
                #     # paciente.save()
                #     pass

    except Exception as e:
        print e
