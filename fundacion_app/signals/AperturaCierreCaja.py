# -*- coding: utf-8 -*-
from django.dispatch import receiver
from django.db.models.signals import post_save
from fundacion_app.models import AperturaCierreCaja, Caja, MovimientoCaja

##  APERTURA CIERRE SIGNALS

##  ANTES SE UTILIZABA PORQUE SOLO EXISTIA UNA APERTURA A LA VEZ. AHORA CUALQUIER USUARIO QUE TENGA
##  ACCESO A LA CAJA PUEDE CREAR SU PROPIA APERTURA AISLADA DE LAS DE OTROS USUARIOS
# @receiver(post_save, sender=AperturaCierreCaja)
# def aperturar_cerrar_caja(sender, instance, created, **kwargs):
#     caja = Caja.objects.first()

#     if created:
#         ##  SI SE APERTURA LA CAJA SE DEBE CAMBIAR EL ESTADO A ABIERTA
#         caja.estado = Caja.APERTURADA
#         caja.save()

#     elif instance.estado == Caja.CERRADA:
#         ##  SI SE RECIBIO UN ESTADO CERRADO, CAMBIAR EL ESTADO DE LA CAJA A CERRADO
#         caja.estado = Caja.CERRADA
#         caja.save()

#     else:
#         pass


##  MOVIMIENTO DE CAJA SIGNALS
@receiver(post_save, sender=MovimientoCaja)
def movimiento_caja(sender, instance, created, **kwargs):
    ##  ACTUALIZA EL MONTO ACTUAL DE LA APERTURA DE CAJA SI SE REALIZO UN INGRESO O EGRESO
    if created:
        mc = MovimientoCaja.objects.get(pk=instance.id)
        ac = mc.apertura_cierre_caja

        if str(mc.tipo) == str(MovimientoCaja.INGRESO):
            ac.total_actual += mc.cantidad
            ac.save()

        elif str(mc.tipo) == str(MovimientoCaja.EGRESO):
            ac.total_actual -= mc.cantidad
            ac.save()

    else:
        pass
