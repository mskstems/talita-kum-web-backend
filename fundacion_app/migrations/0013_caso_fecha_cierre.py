# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2017-09-28 06:47
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('fundacion_app', '0012_auto_20170927_2156'),
    ]

    operations = [
        migrations.AddField(
            model_name='caso',
            name='fecha_cierre',
            field=models.DateTimeField(blank=True, null=True),
        ),
    ]
