# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2017-10-15 01:33
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('fundacion_app', '0018_auto_20171014_1911'),
    ]

    operations = [
        migrations.AddField(
            model_name='caja',
            name='saldo_actual',
            field=models.FloatField(blank=True, default=0, null=True),
        ),
    ]
