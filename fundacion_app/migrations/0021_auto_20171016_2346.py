# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2017-10-17 05:46
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('fundacion_app', '0020_auto_20171016_2041'),
    ]

    operations = [
        migrations.AlterField(
            model_name='aperturacierrecaja',
            name='caja',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='aperturas_cierres_caja', to='fundacion_app.Caja'),
        ),
    ]
