# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.apps import AppConfig

class FundacionAppConfig(AppConfig):
    name = 'fundacion_app'

    def ready(self):
        ##  SIGNALS
        from fundacion_app.signals import Perfil, AperturaCierreCaja, CobrosPaciente
