# -*- coding: utf-8 -*-
from rest_framework import serializers
from fundacion_app.models import Caso
from fundacion_app.serializers import PerfilSerializerPopulate, PacienteSerializerPopulate
from fundacion_app.models import NotaEvolutiva

class NotaEvolutivaSerializer(serializers.ModelSerializer):
    class Meta:
        model = NotaEvolutiva
        fields = (
            '__all__'
        )

class CasoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Caso
        fields = (
            '__all__'
        )

class CasoSerializerPopulate(serializers.ModelSerializer):
    colaborador = PerfilSerializerPopulate(read_only=True)
    paciente = PacienteSerializerPopulate(read_only=True)
    notas_evolutivas = NotaEvolutivaSerializer(read_only=True, many=True)
    class Meta:
        model = Caso
        fields = (
            '__all__'
        )
