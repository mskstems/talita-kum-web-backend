# -*- coding: utf-8 -*-
from rest_framework import serializers
from fundacion_app.models import Profesion

class ProfesionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Profesion
        fields = (
            'id',
            'profesion',
            'fecha_creacion',
            'fecha_modificacion'
        )
