# -*- coding: utf-8 -*-
from rest_framework import serializers
from fundacion_app.models import NotaEvolutiva
from fundacion_app.serializers import CasoSerializerPopulate

class NotaEvolutivaSerializer(serializers.ModelSerializer):
    class Meta:
        model = NotaEvolutiva
        fields = (
            '__all__'
        )

class NotaEvolutivaSerializerPopulate(serializers.ModelSerializer):
    caso = CasoSerializerPopulate(read_only=True)
    class Meta:
        model = NotaEvolutiva
        fields = (
            '__all__'
        )
