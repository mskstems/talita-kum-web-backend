# -*- coding: utf-8 -*-
from rest_framework import serializers
from fundacion_app.models import Perfil
from rest_framework.fields import MultipleChoiceField
from fundacion_app.serializers import UserSerializer, PersonaSerializer, ProfesionSerializer

class PerfilSerializerRetrieve(serializers.ModelSerializer):
    "Get"
    user = UserSerializer(read_only=True)
    persona = PersonaSerializer(read_only=True)
    profesion = ProfesionSerializer(read_only=True)
    roles = MultipleChoiceField(Perfil.ROLES)
    class Meta:
        model = Perfil
        fields = (
            'id',
            'user',
            'persona',
            'estado',
            'roles',
            'profesion',
            'colegiado',
            'eliminado',
            'fecha_creacion',
            'fecha_modificacion'
        )

class PerfilSerializerPopulate(serializers.ModelSerializer):
    "List"
    user = UserSerializer(read_only=True)
    persona = PersonaSerializer(read_only=True)
    profesion = ProfesionSerializer(read_only=True)
    roles = serializers.SerializerMethodField()
    class Meta:
        model = Perfil
        fields = (
            'id',
            'user',
            'persona',
            'estado',
            'roles',
            'profesion',
            'colegiado',
            'eliminado',
            'fecha_creacion',
            'fecha_modificacion'
        )

    def get_roles(self, obj):
        tmp = obj.get_roles_display()
        tmp = tmp.title() if tmp else tmp
        return tmp

class PerfilSerializer(serializers.ModelSerializer):
    "Create, update or delete"
    roles = MultipleChoiceField(Perfil.ROLES)
    class Meta:
        model = Perfil
        fields = (
            'id',
            'user',
            'persona',
            'estado',
            'roles',
            'profesion',
            'colegiado',
            'eliminado',
            'fecha_creacion',
            'fecha_modificacion'
        )
