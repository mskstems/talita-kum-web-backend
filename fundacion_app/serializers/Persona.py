# -*- coding: utf-8 -*-
from rest_framework import serializers
from fundacion_app.models import Persona

class PersonaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Persona
        fields = (
            'id',
            'persona',
            'apellido_casada',
            'cui',
            'telefono',
            'celular',
            'nacionalidad',
            'departamento',
            'direccion',
            'correo',
            'foto',
            'fecha_creacion',
            'fecha_modificacion'
        )
