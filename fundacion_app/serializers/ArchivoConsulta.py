# -*- coding: utf-8 -*-
from rest_framework import serializers
from fundacion_app.models import ArchivoConsulta
from fundacion_app.serializers import ConsultaSerializerPopulate

class ArchivoConsultaSerializer(serializers.ModelSerializer):
    class Meta:
        model = ArchivoConsulta
        fields = (
            '__all__'
        )

class ArchivoConsultaSerializerPopulate(serializers.ModelSerializer):
    consulta = ConsultaSerializerPopulate(read_only=True)
    class Meta:
        model = ArchivoConsulta
        fields = (
            '__all__'
        )
