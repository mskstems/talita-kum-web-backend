# -*- coding: utf-8 -*-
from rest_framework import serializers
from fundacion_app.models import Cita
from fundacion_app.serializers import PerfilSerializerPopulate, PacienteSerializerPopulate

class CitaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Cita
        fields = (
            '__all__'
        )

class CitaSerializerPopulate(serializers.ModelSerializer):
    colaborador = PerfilSerializerPopulate(read_only=True)
    paciente = PacienteSerializerPopulate(read_only=True)
    class Meta:
        model = Cita
        fields = (
            '__all__'
        )
