# -*- coding: utf-8 -*-
from rest_framework import serializers
from fundacion_app.models import Caja

class CajaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Caja
        fields = (
            '__all__'
        )
