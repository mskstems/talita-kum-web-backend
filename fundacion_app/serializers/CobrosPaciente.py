# -*- coding: utf-8 -*-
from rest_framework import serializers
from fundacion_app.models import CobrosPaciente
from fundacion_app.serializers import PerfilSerializerPopulate, PacienteSerializerPopulate

class CobrosPacienteSerializer(serializers.ModelSerializer):
    class Meta:
        model = CobrosPaciente
        fields = (
            '__all__'
        )

class CobrosPacienteSerializerPopulate(serializers.ModelSerializer):
    paciente = PacienteSerializerPopulate(read_only=True)
    colaborador = PerfilSerializerPopulate(read_only=True)
    class Meta:
        model = CobrosPaciente
        fields = (
            '__all__'
        )
