# -*- coding: utf-8 -*-
from rest_framework import serializers
from django.contrib.auth.models import User

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = (
            'id',
            #'first_name',
            #'email',
            'username',
            #'password'
        )

"""
#from fundacion_app.serializers import PerfilSerializer
class UserSerializerPopulate(serializers.ModelSerializer):
    perfil = PerfilSerializer()
    class Meta:
        model = User
        fields = (
            'id',
            'first_name',
            'email',
            'username',
            #'is_superuser',
            #'is_staff',
            #'date_joined',
            'perfil',
        )

class UserWriteSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username', 'is_superuser', 'password', 'first_name', 'email')


class UserNameSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'first_name', 'email')
"""
