# -*- coding: utf-8 -*-
from rest_framework import serializers
from fundacion_app.models import Reconsulta
from fundacion_app.serializers import ConsultaSerializerPopulate

class ReconsultaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Reconsulta
        fields = (
            '__all__'
        )

class ReconsultaSerializerPopulate(serializers.ModelSerializer):
    consulta = ConsultaSerializerPopulate(read_only=True)
    class Meta:
        model = Reconsulta
        fields = (
            '__all__'
        )
