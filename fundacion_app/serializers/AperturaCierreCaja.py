# -*- coding: utf-8 -*-
from rest_framework import serializers
from fundacion_app.models import AperturaCierreCaja, Caja, Perfil
from fundacion_app.serializers import PerfilSerializerPopulate, CajaSerializer

class AperturaCierreCajaSerializer(serializers.ModelSerializer):
    class Meta:
        model = AperturaCierreCaja
        fields = (
            '__all__'
        )

class AperturaCierreCajaSerializerPopulate(serializers.ModelSerializer):
    caja = CajaSerializer(read_only=True)
    colaborador = PerfilSerializerPopulate(read_only=True)
    class Meta:
        model = AperturaCierreCaja
        fields = (
            '__all__'
        )
