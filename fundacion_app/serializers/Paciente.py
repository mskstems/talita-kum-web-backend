# -*- coding: utf-8 -*-
from rest_framework import serializers
from fundacion_app.models import Paciente, Persona
from rest_framework.fields import MultipleChoiceField
from fundacion_app.serializers import PersonaSerializer

class PacienteSerializerPopulate(serializers.ModelSerializer):
    "List, Get"
    persona = PersonaSerializer(read_only=True)
    tipos = serializers.SerializerMethodField()
    class Meta:
        model = Paciente
        fields = (
            "__all__"
        )

    def get_tipos(self, obj):
        tmp = obj.get_tipos_display()
        tmp = tmp.title() if tmp else tmp
        return tmp

class PacienteSerializer(serializers.ModelSerializer):
    "Create, update or delete"
    tipos = MultipleChoiceField(Paciente.TIPOS)
    persona = PersonaSerializer(read_only=False)
    class Meta:
        model = Paciente
        fields = (
            "__all__"
        )

    def create(self, validated_data):
        "Post API method"
        data_persona = validated_data.pop('persona')
        persona = Persona.objects.create(**data_persona)
        paciente = Paciente.objects.create(persona_id=persona.id, **validated_data)
        return Paciente.objects.get(pk=paciente.id)

    def update(self, instance, validated_data):
        "Put API method"
        data_persona = validated_data.pop('persona')
        Persona.objects.filter(pk=instance.persona.id).update(**data_persona)
        Paciente.objects.filter(pk=instance.id).update(**validated_data)
        return Paciente.objects.get(pk=instance.id)
