# -*- coding: utf-8 -*-
from rest_framework import serializers
from fundacion_app.models import MovimientoCaja, Caja, Perfil
from fundacion_app.serializers import AperturaCierreCajaSerializerPopulate

class MovimientoCajaSerializer(serializers.ModelSerializer):
    class Meta:
        model = MovimientoCaja
        fields = (
            '__all__'
        )

class MovimientoCajaSerializerPopulate(serializers.ModelSerializer):
    apertura_cierre_caja = AperturaCierreCajaSerializerPopulate(read_only=True)
    class Meta:
        model = MovimientoCaja
        fields = (
            '__all__'
        )
