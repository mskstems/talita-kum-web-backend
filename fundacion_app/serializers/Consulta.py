# -*- coding: utf-8 -*-
from rest_framework import serializers
from fundacion_app.models import Consulta, Reconsulta
from fundacion_app.serializers import PerfilSerializerPopulate, PacienteSerializerPopulate

class ReconsultaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Reconsulta
        fields = (
            '__all__'
        )

class ConsultaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Consulta
        fields = (
            '__all__'
        )

class ConsultaSerializerPopulate(serializers.ModelSerializer):
    colaborador = PerfilSerializerPopulate(read_only=True)
    paciente = PacienteSerializerPopulate(read_only=True)
    reconsultas = ReconsultaSerializer(read_only=True, many=True)
    class Meta:
        model = Consulta
        fields = (
            '__all__'
        )
