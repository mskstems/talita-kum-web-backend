# -*- coding: utf-8 -*-
from django.core.files import File
from fundacion_app.models import Paciente
from rest_framework.response import Response
from rest_framework.decorators import list_route
from rest_framework import viewsets, status, filters
from django.core.files.uploadedfile import InMemoryUploadedFile
from fundacion_app.serializers import PacienteSerializer, PacienteSerializerPopulate

class PacienteViewSet(viewsets.ModelViewSet):
    "Interfaz de programación de aplicaciónes para Pacientes"
    queryset = Paciente.objects.filter()
    #permission_classes = [AdminPermission]
    filter_backends = (filters.DjangoFilterBackend, filters.SearchFilter, filters.OrderingFilter)
    AVAILABLE_FILTERS = (
        'id',
        'persona__persona',
        'persona__cui',
        'persona__telefono',
        'persona__celular',
        'persona__direccion',
        'persona__correo',
        'contacto_referencia',
        'telefono_referencia',
        'religion',
        'tipo_de_sangre',
        'num_hermanos',
        'genero',
        'nivel_academico',
        'ocupacion',
        'lugar_nacimiento',
        'nombres_padres',
    )

    filter_fields = AVAILABLE_FILTERS
    search_fields = AVAILABLE_FILTERS
    ordering_fields = AVAILABLE_FILTERS

    def get_queryset(self):
        "Query selector"
        return self.queryset.filter(eliminado=False).order_by('-fecha_creacion')

    def get_serializer_class(self):
        "Define serializer for API"
        if self.action == "list":
            return PacienteSerializerPopulate
        elif self.action == "retrieve":
            return PacienteSerializerPopulate
        else:
            return PacienteSerializer

    @list_route(methods=['post'])
    def guardar_foto(self, request, *args, **kwargs):
        "Guardar la imagen de un paciente con el id de paciente"
        try:
            id = request.data.get('id', None)
            foto = request.data.get('foto', None)

            if id and type(foto) is InMemoryUploadedFile:
                paciente = Paciente.objects.get(pk=id)
                persona = paciente.persona
                persona.foto = File(foto)
                persona.save()

            return Response(status=status.HTTP_200_OK)

        except Exception as e:
            print e
            return Response({ 'detail': str(e) }, status=status.HTTP_400_BAD_REQUEST)

    def destroy(self, request, *args, **kwargs):
        "Paciente deactivator (Soft Delete)"
        try:
            paciente = self.get_object()
            paciente.eliminado = True
            paciente.save()
            return Response(status=status.HTTP_204_NO_CONTENT)

        except Exception as e:
            print e
            return Response({ 'detail': str(e) }, status=status.HTTP_400_BAD_REQUEST)
