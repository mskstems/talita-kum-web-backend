# -*- coding: utf-8 -*-
from datetime import datetime
from django.utils import timezone
from rest_framework.response import Response
from rest_framework.decorators import list_route
from rest_framework.exceptions import APIException
from rest_framework import viewsets, status, filters
from fundacion_app.authorization import AnyPermission
from fundacion_app.utils.utils import determinar_ingresos_egresos
from fundacion_app.models import AperturaCierreCaja, Caja, MovimientoCaja, Perfil
from fundacion_app.utils.pdf_caja import pdf_cierre_caja, pdf_estado_cuenta_caja, pdf_reporte_movimientos_caja
from fundacion_app.serializers import (
    AperturaCierreCajaSerializer,
    AperturaCierreCajaSerializerPopulate,
    MovimientoCajaSerializer,
    MovimientoCajaSerializerPopulate,
    PerfilSerializerPopulate
)

class AperturaCierreCajaViewSet(viewsets.ModelViewSet):
    "Interfaz de programación de aplicaciónes para AperturaCierreCaja"
    queryset = AperturaCierreCaja.objects.filter().order_by('-fecha_creacion')
    #permission_classes = [AdminPermission]
    filter_backends = (filters.DjangoFilterBackend, filters.SearchFilter, filters.OrderingFilter)
    filter_fields = ('id', 'estado')
    search_fields = ('id', 'colaborador__persona__persona')
    ordering_fields = ('id', 'id')

    def get_queryset(self):
        "Query selector"
        # return self.queryset.filter().order_by('-fecha_creacion')
        perfil = self.request.user.perfil
        if perfil:
            if Perfil.ADMIN in perfil.roles:
                return self.queryset
            else:
                return self.queryset.filter(colaborador_id=perfil.id)
        else:
            return self.queryset

    def get_serializer_class(self):
        "Define serializer for API"
        if self.action == "list":
            return AperturaCierreCajaSerializerPopulate
        elif self.action == "retrieve":
            return AperturaCierreCajaSerializerPopulate
        else:
            return AperturaCierreCajaSerializer

    def perform_create(self, serializer):
        "Asigna el usuario logueado al aperturar la caja"
        ##  VERIFICAR QUE NO HAYA OTRA APERTURA DE CAJA. SI LA HUBIERA NO SE PUEDE ABRIR LA CAJA
        if not AperturaCierreCaja.objects \
            .filter(estado=Caja.APERTURADA, colaborador_id=self.request.user.perfil.id) \
            .count():
            serializer.save(colaborador=self.request.user.perfil, caja=Caja.objects.first(), estado=Caja.APERTURADA)
        else:
            raise APIException("No es posible aperturar la caja porque ya ha aperturado una antes.")

    @list_route(methods=['get'])
    def ingresos_egresos(self, request, *args, **kwargs):
        "Obtener los ingresos y egresos del id de la apertura_cierre_caja recibida"
        try:
            apertura_cierre_id = request.GET.get('id', None)

            if not apertura_cierre_id:
                raise Exception("No fue enviado el ID de la caja")

            result = determinar_ingresos_egresos(apertura_cierre_id)
            return Response(result, status=status.HTTP_200_OK)

        except Exception as e:
            print e
            return Response({ 'detail': str(e) }, status=status.HTTP_400_BAD_REQUEST)


    @list_route(methods=['get'], permission_classes=[AnyPermission])
    def pdf_cierre_caja(self, request, *args, **kwargs):
        "Obtiene el informe del cierre de caja"
        try:
            apertura_cierre_id = request.GET.get('id', None)

            apertura_cierre = AperturaCierreCaja.objects \
                .filter(pk=apertura_cierre_id) \
                .prefetch_related('colaborador__persona') \
                .first()

            serializer = AperturaCierreCajaSerializerPopulate(apertura_cierre)
            apertura_cierre_dict = serializer.data

            apertura_cierre_dict["ingresos_egresos"] = determinar_ingresos_egresos(apertura_cierre_id)

            ##  EL PDF DEVUELVE UN RESPONSE PARA RENDERIZARLO EN NAVEGADOR
            return pdf_cierre_caja(request, apertura_cierre, apertura_cierre_dict)

        except Exception as e:
            print e
            return Response(str(e), status=status.HTTP_404_NOT_FOUND)

    @list_route(methods=['get'], permission_classes=[AnyPermission])
    def pdf_estado_cuenta_caja(self, request, *args, **kwargs):
        "Devuelve el PDF del estado de cuenta (movimientos realizados) de una apertura de caja"
        try:
            apertura_cierre_id = request.GET.get('id', None)

            ##  OBTENER CIERRE DE CAJA
            apertura_cierre = AperturaCierreCaja.objects \
                .filter(pk=apertura_cierre_id) \
                .prefetch_related('colaborador__persona') \
                .first()

            ##  OBTENER LOS MOVIMIENTOS DE CAJA PERTENECIENTES A ESTA CIERRE DE CAJA Y PARSEARLOS A DICT
            movimientos_caja = MovimientoCaja.objects.filter(apertura_cierre_caja_id=apertura_cierre_id)
            movimientos_caja_dict = MovimientoCajaSerializer(movimientos_caja, many=True)

            serializer = AperturaCierreCajaSerializerPopulate(apertura_cierre)
            apertura_cierre_dict = serializer.data
            apertura_cierre_dict['movimientos'] = movimientos_caja_dict.data
            apertura_cierre_dict["ingresos_egresos"] = determinar_ingresos_egresos(apertura_cierre_id)

            ##  EL PDF DEVUELVE UN RESPONSE PARA RENDERIZARLO EN NAVEGADOR
            return pdf_estado_cuenta_caja(request, apertura_cierre, apertura_cierre_dict)

        except Exception as e:
            print e
            return Response(str(e), status=status.HTTP_404_NOT_FOUND)

    @list_route(methods=['get'], permission_classes=[AnyPermission])
    def reporte_movimientos(self, request, *args, **kwargs):
        "Generador del reporte PDF de todos los movimientos realizados"
        try:

            colaborador_id = request.GET.get('usuario', None)
            fecha_inicial = request.GET.get('fecha_inicial', None)
            fecha_final = request.GET.get('fecha_final', None)

            fecha_inicial = datetime.strptime(fecha_inicial, "%Y-%m-%d")
            fecha_final = datetime.strptime(fecha_final, "%Y-%m-%d")
            fecha_inicial = timezone.make_aware(fecha_inicial)
            fecha_final = timezone.make_aware(fecha_final)

            colaborador = Perfil.objects.get(pk=colaborador_id)
            colaborador_dict = PerfilSerializerPopulate(colaborador)

            ##  OBTENER LOS MOVIMIENTOS DE CAJA PERTENECIENTES A ESTA CIERRE DE CAJA Y PARSEARLOS A DICT
            movimientos_caja = MovimientoCaja.objects \
                .filter(fecha_creacion__gte=fecha_inicial, fecha_creacion__lte=fecha_final) \
                .order_by('fecha_creacion')
            movimientos_caja_dict = MovimientoCajaSerializerPopulate(movimientos_caja, many=True)

            info = {
                "colaborador": colaborador_dict.data,
                "fecha_inicial": fecha_inicial.strftime("%d-%m-%Y"),
                "fecha_final": fecha_final.strftime("%d-%m-%Y"),
            }

            return pdf_reporte_movimientos_caja(request, info, movimientos_caja, movimientos_caja_dict.data)

            # ##  EL PDF DEVUELVE UN RESPONSE PARA RENDERIZARLO EN NAVEGADOR
            # return pdf_estado_cuenta_caja(request, apertura_cierre, apertura_cierre_dict)

        except Exception as e:
            print e
            return Response(str(e), status=status.HTTP_404_NOT_FOUND)

    def destroy(self, request, *args, **kwargs):
        "AperturaCierreCaja -- No se puede eliminar"
        return Response(
            { "detail": "La apertura o cierre de caja no puede ser eliminada." },
            status=status.HTTP_400_BAD_REQUEST
        )
