# -*- coding: utf-8 -*-
from django.utils import timezone
from rest_framework.response import Response
from rest_framework.decorators import list_route
from rest_framework import viewsets, status, filters
from fundacion_app.models import Caja
from fundacion_app.serializers import CajaSerializer

class CajaViewSet(viewsets.ModelViewSet):
    "Interfaz de programación de aplicaciónes para Caja"
    queryset = Caja.objects.filter().order_by('-id')
    serializer_class = CajaSerializer
    filter_backends = (filters.DjangoFilterBackend, filters.SearchFilter, filters.OrderingFilter)
    filter_fields = ('id', 'id')
    search_fields = ('id', 'id')
    ordering_fields = ('id', 'id')

    def destroy(self, request, *args, **kwargs):
        "Caja -- No se puede eliminar la caja"
        return Response({ "detail": "La caja no puede ser eliminada." }, status=status.HTTP_400_BAD_REQUEST)
