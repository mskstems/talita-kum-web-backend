# -*- coding: utf-8 -*-
import json
from django.core.files import File
from rest_framework.response import Response
from rest_framework.decorators import list_route
from rest_framework import viewsets, status, filters
from fundacion_app.authorization import AnyPermission
from django.core.files.uploadedfile import InMemoryUploadedFile
from fundacion_app.utils.pdf_consultas import pdf_tratamiento, pdf_expediente
from fundacion_app.models import Consulta, Perfil, ArchivoConsulta, Reconsulta
from fundacion_app.serializers import (
    ConsultaSerializer,
    ConsultaSerializerPopulate,
    ArchivoConsultaSerializer,
    ArchivoConsultaSerializerPopulate,
    ReconsultaSerializerPopulate
)

import sys
reload(sys)
sys.setdefaultencoding('utf8')

class ConsultaViewSet(viewsets.ModelViewSet):
    "Interfaz de programación de aplicaciónes para Consultas"
    queryset = Consulta.objects.filter(eliminado=False).order_by('-fecha_creacion')
    filter_backends = (filters.DjangoFilterBackend, filters.SearchFilter, filters.OrderingFilter)
    filter_fields = ('id', 'colaborador', 'paciente')
    search_fields = ('id', 'colaborador__persona__persona', 'paciente__persona__persona')
    ordering_fields = ('id', 'colaborador', 'paciente')

    def get_queryset(self):
        "Query selector"
        # return self.queryset
        perfil = self.request.user.perfil
        if perfil:
            if Perfil.ADMIN in perfil.roles or Perfil.SECRETARIA in perfil.roles:
                # return self.queryset
                ##  SE DEJA DE ESTA MANERA PORQUE CADA COLABORADOR EXCLUSIVAMENTE PUEDE VER SUS CONSULTAS
                ##  NINGÚN OTRO USUARIO LAS PODRÁ VISUALIZAR YA QUE ES INFORMACIÓN CORRESPONDIENTE DE ÉL
                return self.queryset.filter(colaborador_id=perfil.id)
            else:
                return self.queryset.filter(colaborador_id=perfil.id)
        else:
            return self.queryset

    def get_serializer_class(self):
        "Define serializer for API"
        if self.action == "list" or self.action == "retrieve":
            return ConsultaSerializerPopulate
        else:
            return ConsultaSerializer

    def perform_create(self, serializer):
        "Parsea la cadena de notas de unicode a string"
        notas = str(self.request.data.get('notas', '{}'))
        serializer.save(notas=notas)

    def perform_update(self, serializer):
        "Parsea la cadena de notas de unicode a string"
        notas = str(self.request.data.get('notas', '{}'))
        serializer.save(notas=notas)

    @list_route(methods=['get'], permission_classes=[AnyPermission])
    def pdf_tratamiento(self, request, *args, **kwargs):
        "Obtiene el PDF del tratamiento - receta del médico"
        try:
            consulta_id = request.GET.get('id', None)

            consulta = Consulta.objects \
                .filter(pk=consulta_id) \
                .prefetch_related('colaborador__persona', 'paciente__persona') \
                .first()

            serializer = ConsultaSerializerPopulate(consulta)
            consulta_dict = serializer.data
            consulta_dict['notas'] = json.loads(consulta_dict['notas'])

            ##  EL PDF DEVUELVE UN RESPONSE PARA RENDERIZARLO EN NAVEGADOR
            return pdf_tratamiento(request, consulta_dict)

        except Exception as e:
            print e
            return Response(str(e), status=status.HTTP_404_NOT_FOUND)

    @list_route(methods=['get'], permission_classes=[AnyPermission])
    def pdf_expediente(self, request, *args, **kwargs):
        "Obtiene el PDF del tratamiento - receta del médico"
        try:
            consulta_id = request.GET.get('id', None)

            consulta = Consulta.objects \
                .filter(pk=consulta_id) \
                .prefetch_related('colaborador__persona', 'paciente__persona') \
                .first()

            serializer = ConsultaSerializerPopulate(consulta)
            consulta_dict = serializer.data
            consulta_dict['notas'] = json.loads(consulta_dict['notas'])

            consulta_dict['paciente']['notas_medicas'] = json.loads(consulta_dict['paciente']['notas_medicas'])

            ##  ARRAY DE RECONSULTAS
            reconsultas = Reconsulta.objects \
                .filter(consulta_id=consulta.id) \
                .order_by('fecha_creacion')

            serializer_reconsultas = ReconsultaSerializerPopulate(reconsultas, many=True)
            reconsultas_dict = serializer_reconsultas.data
            for reconsulta in reconsultas_dict:
                reconsulta['notas'] = json.loads(reconsulta['notas'])

            ##  EL PDF DEVUELVE UN RESPONSE PARA RENDERIZARLO EN NAVEGADOR
            return pdf_expediente(request, consulta, consulta_dict, reconsultas, reconsultas_dict)

        except Exception as e:
            print e
            return Response(str(e), status=status.HTTP_404_NOT_FOUND)

    def destroy(self, request, *args, **kwargs):
        "Consulta soft delete"
        try:
            consulta = self.get_object()
            consulta.eliminado = True
            consulta.save()
            return Response(status=status.HTTP_204_NO_CONTENT)

        except Exception as e:
            print e
            return Response({ 'detail': str(e) }, status=status.HTTP_400_BAD_REQUEST)
