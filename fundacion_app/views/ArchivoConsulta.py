# -*- coding: utf-8 -*-
from django.core.files import File
from rest_framework.response import Response
from rest_framework.decorators import list_route
from rest_framework import viewsets, status, filters
from fundacion_app.authorization import AnyPermission
from django.core.files.uploadedfile import InMemoryUploadedFile
from fundacion_app.models import Consulta, Perfil, ArchivoConsulta
from fundacion_app.serializers import ArchivoConsultaSerializer, ArchivoConsultaSerializerPopulate

class ArchivoConsultaViewSet(viewsets.ModelViewSet):
    "Interfaz de programación de aplicaciónes para Archivos de Consultas"
    queryset = ArchivoConsulta.objects.filter(eliminado=False).order_by('-fecha_creacion')
    filter_backends = (filters.DjangoFilterBackend, filters.SearchFilter, filters.OrderingFilter)
    filter_fields = ('id', 'id')
    search_fields = ('id', 'id')
    ordering_fields = ('id', 'id')

    def get_queryset(self):
        "Query selector"
        return self.queryset

    def get_serializer_class(self):
        "Define serializer for API"
        if self.action == "list" or self.action == "retrieve":
            return ArchivoConsultaSerializerPopulate
        else:
            return ArchivoConsultaSerializer

    def list(self, request, *args, **kwargs):
        "Personalizando Query para que devuelva todos sin paginación"
        results = []
        try:
            consulta = request.GET.get('consulta', None)

            if consulta:
                query = ArchivoConsulta.objects.filter(eliminado=False, consulta_id=consulta).order_by('-fecha_creacion')
                serializer = ArchivoConsultaSerializer(query, context={ "request": request }, many=True)
                results = serializer.data

        except Exception as e:
            results = []

        return Response({ 'count': len(results), 'results': results }, status=status.HTTP_200_OK)

    @list_route(methods=['post'])
    def guardar_archivo(self, request, *args, **kwargs):
        "Guardar el archivo de consulta"
        try:
            consulta = request.data.get('consulta', None)
            archivo = request.data.get('archivo', None)

            if consulta and type(archivo) is InMemoryUploadedFile:
                ArchivoConsulta.objects.create(consulta_id=consulta, archivo=File(archivo))

            return Response(status=status.HTTP_200_OK)

        except Exception as e:
            print e
            return Response({ 'detail': str(e) }, status=status.HTTP_400_BAD_REQUEST)

    def destroy(self, request, *args, **kwargs):
        "ArchivoConsulta soft delete"
        try:
            model = self.get_object()
            model.eliminado = True
            model.save()
            return Response(status=status.HTTP_204_NO_CONTENT)

        except Exception as e:
            print e
            return Response({ 'detail': str(e) }, status=status.HTTP_400_BAD_REQUEST)
