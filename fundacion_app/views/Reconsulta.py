# -*- coding: utf-8 -*-
import json
from rest_framework.response import Response
from rest_framework.decorators import list_route
from fundacion_app.models import Reconsulta, Perfil
from rest_framework import viewsets, status, filters
from fundacion_app.authorization import AnyPermission
from fundacion_app.utils.pdf_reconsultas import pdf_tratamiento, pdf_expediente
from fundacion_app.serializers import ReconsultaSerializer, ReconsultaSerializerPopulate

import sys
reload(sys)
sys.setdefaultencoding('utf8')

class ReconsultaViewSet(viewsets.ModelViewSet):
    "Interfaz de programación de aplicaciónes para Reconsultas"
    queryset = Reconsulta.objects.filter(eliminado=False).order_by('-fecha_creacion')
    filter_backends = (filters.DjangoFilterBackend, filters.SearchFilter, filters.OrderingFilter)
    filter_fields = ('id', 'consulta', 'consulta__colaborador', 'consulta__paciente')
    search_fields = ('id', 'motivo', 'consulta__paciente__persona__persona')
    ordering_fields = ('id', 'id')

    def get_queryset(self):
        "Query selector"
        return self.queryset

    def get_serializer_class(self):
        "Define serializer for API"
        if self.action == "list" or self.action == "retrieve":
            return ReconsultaSerializerPopulate
        else:
            return ReconsultaSerializer

    def perform_create(self, serializer):
        "Parsea la cadena de notas de unicode a string"
        notas = str(self.request.data.get('notas', '{}'))
        serializer.save(notas=notas)

    def perform_update(self, serializer):
        "Parsea la cadena de notas de unicode a string"
        notas = str(self.request.data.get('notas', '{}'))
        serializer.save(notas=notas)

    @list_route(methods=['get'], permission_classes=[AnyPermission])
    def pdf_tratamiento(self, request, *args, **kwargs):
        "Obtiene el PDF del tratamiento - receta del médico"
        try:
            reconsulta_id = request.GET.get('id', None)

            reconsulta = Reconsulta.objects \
                .filter(pk=reconsulta_id) \
                .prefetch_related('consulta__colaborador__persona', 'consulta__paciente__persona') \
                .first()

            serializer = ReconsultaSerializerPopulate(reconsulta)
            reconsulta_dict = serializer.data
            reconsulta_dict['notas'] = json.loads(reconsulta_dict['notas'])

            ##  EL PDF DEVUELVE UN RESPONSE PARA RENDERIZARLO EN NAVEGADOR
            return pdf_tratamiento(request, reconsulta_dict)

        except Exception as e:
            print e
            return Response(str(e), status=status.HTTP_404_NOT_FOUND)

    @list_route(methods=['get'], permission_classes=[AnyPermission])
    def pdf_expediente(self, request, *args, **kwargs):
        "Obtiene el PDF del tratamiento - receta del médico"
        try:
            reconsulta_id = request.GET.get('id', None)

            reconsulta = Reconsulta.objects \
                .filter(pk=reconsulta_id) \
                .prefetch_related('consulta__colaborador__persona', 'consulta__paciente__persona') \
                .first()

            serializer = ReconsultaSerializerPopulate(reconsulta)
            reconsulta_dict = serializer.data
            reconsulta_dict['notas'] = json.loads(reconsulta_dict['notas'])

            reconsulta_dict['consulta']['paciente']['notas_medicas'] = json.loads(
                reconsulta_dict['consulta']['paciente']['notas_medicas']
            )

            ##  EL PDF DEVUELVE UN RESPONSE PARA RENDERIZARLO EN NAVEGADOR
            return pdf_expediente(request, reconsulta, reconsulta_dict)

        except Exception as e:
            print e
            return Response(str(e), status=status.HTTP_404_NOT_FOUND)

    def destroy(self, request, *args, **kwargs):
        "Reconsulta soft delete"
        try:
            consulta = self.get_object()
            consulta.eliminado = True
            consulta.save()
            return Response(status=status.HTTP_204_NO_CONTENT)

        except Exception as e:
            print e
            return Response({ 'detail': str(e) }, status=status.HTTP_400_BAD_REQUEST)
