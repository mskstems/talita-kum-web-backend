# -*- coding: utf-8 -*-
from django.core.files import File
from rest_framework.response import Response
from rest_framework.decorators import list_route
from rest_framework import viewsets, status, filters
from fundacion_app.models import Caso, Perfil, NotaEvolutiva
from django.core.files.uploadedfile import InMemoryUploadedFile
from fundacion_app.serializers import NotaEvolutivaSerializer, NotaEvolutivaSerializerPopulate

class NotaEvolutivaViewSet(viewsets.ModelViewSet):
    "Interfaz de programación de aplicaciónes para Archivos de Consultas"
    queryset = NotaEvolutiva.objects.filter(anulada=False).order_by('-fecha_creacion')
    filter_backends = (filters.DjangoFilterBackend, filters.SearchFilter, filters.OrderingFilter)
    filter_fields = ('id', 'caso')
    search_fields = ('id', 'notas', 'observaciones')
    ordering_fields = ('id', 'id')

    def get_queryset(self):
        "Query selector"
        return self.queryset

    def get_serializer_class(self):
        "Define serializer for API"
        if self.action == "list" or self.action == "retrieve":
            return NotaEvolutivaSerializerPopulate
        else:
            return NotaEvolutivaSerializer

    @list_route(methods=['get'])
    def previo(self, request, *args, **kwargs):
        "Obtener el caso previo o el ultimo caso en caso de no recibir ningun id"
        try:
            id = request.GET.get('id', None)
            id_caso = request.GET.get('caso', None)

            query = None
            if id_caso:
                query = NotaEvolutiva.objects.filter(caso_id=id_caso).order_by('-fecha_creacion')
                if id:
                    nota_refer = NotaEvolutiva.objects.get(pk=id)
                    query = query.filter(fecha_creacion__lte=nota_refer.fecha_creacion).exclude(pk=id)
                query = query.first()

            serializer = NotaEvolutivaSerializer(query, context={ 'request': request })
            return Response(serializer.data, status=status.HTTP_200_OK)

        except Exception as e:
            print e
            return Response({ 'detail': str(e) }, status=status.HTTP_400_BAD_REQUEST)

    def destroy(self, request, *args, **kwargs):
        "NotaEvolutiva anular"
        try:
            model = self.get_object()
            model.anulada = True
            model.save()
            return Response(status=status.HTTP_204_NO_CONTENT)

        except Exception as e:
            print e
            return Response({ 'detail': str(e) }, status=status.HTTP_400_BAD_REQUEST)
