# -*- coding: utf-8 -*-
import json, operator
from PIL import Image
from StringIO import StringIO
from django.db.models import Q
from django.core.files import File
from django.contrib.auth.models import User
from rest_framework.response import Response
from rest_framework.decorators import list_route
from fundacion_app.utils.utils import make_square
from rest_framework import viewsets, status, filters
from fundacion_app.authorization import AnyPermission
from django.core.files.uploadedfile import InMemoryUploadedFile
from fundacion_app.models import Perfil, AperturaCierreCaja, Caja
from fundacion_app.utils.reducers import guardar_perfil, guardar_persona
from django.contrib.auth.hashers import check_password, make_password
from fundacion_app.serializers import PerfilSerializer, PerfilSerializerPopulate, PerfilSerializerRetrieve

class PerfilViewSet(viewsets.ModelViewSet):
    "Interfaz de programación de aplicaciónes para usuarios y perfiles"
    queryset = Perfil.objects.all()
    #permission_classes = [AdminPermission]
    filter_backends = (filters.DjangoFilterBackend, filters.SearchFilter, filters.OrderingFilter)
    filter_fields = ('id', 'persona__persona', 'persona__telefono', 'persona__celular', 'persona__direccion', 'persona__correo')
    search_fields = ('id', 'persona__persona', 'persona__telefono', 'persona__celular', 'persona__direccion', 'persona__correo')
    ordering_fields = ('id', 'persona__persona', 'persona__telefono', 'persona__celular', 'persona__direccion', 'persona__correo')

    def get_queryset(self):
        "Query selector"
        roles = self.request.GET.get('roles', None)

        if roles:
            ##  OBTENER SOLAMENTE USUARIOS DE LOS ROLES ENVIADOS
            roles = str(roles).split(',')
            self.queryset = self.queryset.filter(reduce(operator.or_, (Q(roles__icontains = item) for item in roles)))

        return self.queryset.filter(eliminado=False).order_by('-fecha_creacion')

    def get_serializer_class(self):
        "Define serializer for API"
        if self.action == "list":
            return PerfilSerializerPopulate
        elif self.action == "retrieve":
            return PerfilSerializerRetrieve
        else:
            return PerfilSerializer

    @list_route(methods=['get'])
    def me(self, request):
        "Get the user already authenticated"
        try:
            caja_activa = AperturaCierreCaja.objects \
                .filter(colaborador_id=request.user.perfil.id, estado=Caja.APERTURADA) \
                .order_by("-fecha_creacion")

            serializer = PerfilSerializerRetrieve(request.user.perfil, context={ 'request': request })
            result = serializer.data
            result['caja_activa'] = True if caja_activa.count() else False
            result['id_apertura_caja'] = caja_activa.first().id if caja_activa.count() else 0
            return Response(result, status=status.HTTP_200_OK)

        except Exception as e:
            print e
            return Response({ "detail": str(e) }, status=status.HTTP_400_BAD_REQUEST)

    def create(self, request, *args, **kwargs):
        "Create user instance"
        try:
            data = request.data.copy()

            ######################################################################
            ##  FORMATEAR TODOS LOS ITEMS RECIBIDOS EN ROLES A TIPO STRING
            ######################################################################
            roles = json.loads(data['roles'])
            roles = [ str(rol) for rol in roles ]
            data['roles'] = roles
            ######################################################################

            ######################################################################
            ##  CREAR AL USUARIO DE LA PERSONA, CON EL CUAL PODRÁ AUTENTICARSE
            ######################################################################
            ##  SI LA BANDERA TIPO ADMIN VIENE EN LOS ROLES, ES UN SUPERUSUARIO
            ##  VERIFICAR QUE NO EXISTA OTRO USERNAME CON EL MISMO NOMBRE
            existente = User.objects.filter(username=str(data['username']).strip())
            if existente.count() > 0:
                raise Exception("Este correo ya está en uso. Por favor elije otro.")

            is_superuser = str(Perfil.ADMIN) in data['roles']
            user = User.objects.create_user(
                email        = data['username'],
                is_staff     = True,
                username     = data['username'],
                password     = data['password'],
                # first_name   = data['persona'],
                is_superuser = is_superuser,
            )
            ######################################################################

            ######################################################################
            ##  ASIGNAR LA DATA CORRESPONDIENTE AL PERFIL DEL USUARIO
            ######################################################################
            guardar_perfil(user.perfil, data)
            ######################################################################

            ######################################################################
            ##  ASIGNAR LA DATA EXTRA RECIBIDA A LA INSTANCIA DE PERSONA
            ######################################################################
            guardar_persona(user.perfil.persona, data)
            ######################################################################

            ######################################################################
            ##  NOTIFICAR QUE EL PERFIL SE CREÓ EXITOSAMENTE
            ######################################################################
            serializer = PerfilSerializerRetrieve(user.perfil)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
            ######################################################################

        except Exception as e:
            print e
            return Response({ "detail": str(e) }, status=status.HTTP_400_BAD_REQUEST)

    def update(self, request, *args, **kwargs):
        "Update user instance"
        try:
            data = request.data.copy()
            perfil = self.get_object()

            ######################################################################
            ##  FORMATEAR TODOS LOS ITEMS RECIBIDOS EN ROLES A TIPO STRING
            ######################################################################
            roles = json.loads(data['roles'])
            roles = [ str(rol) for rol in roles ]
            data['roles'] = roles
            ######################################################################

            ######################################################################
            ##  ACTUALIZAR LA INFORMACIÓN DEL USUARIO
            ######################################################################
            ##  VERIFICAR QUE NO EXISTA OTRO USERNAME CON EL MISMO NOMBRE
            user = perfil.user
            existente = User.objects.filter(username=str(data['username']).strip()).exclude(pk=user.id)
            if existente.count() > 0:
                raise Exception("Este correo ya está en uso. Por favor elije otro.")
            is_superuser      = str(Perfil.ADMIN) in data['roles']
            user.email        = data['username']
            user.username     = data['username']
            # user.first_name   = data['persona']
            user.is_superuser = is_superuser
            user.save()
            ######################################################################

            ######################################################################
            ##  ACTUALIZAR LA DATA CORRESPONDIENTE AL PERFIL DEL USUARIO
            ######################################################################
            guardar_perfil(perfil, data)
            ######################################################################

            ######################################################################
            ##  ACTUALIZAR LA INFORMACIÓN DE LA PERSONA
            ######################################################################
            guardar_persona(perfil.persona, data)
            ######################################################################

            ######################################################################
            ##  NOTIFICAR QUE EL PERFIL SE ACTUAIZÓ EXITOSAMENTE
            ######################################################################
            serializer = PerfilSerializerRetrieve(perfil)
            return Response(serializer.data, status=status.HTTP_200_OK)
            ######################################################################

        except Exception as e:
            print e
            return Response({ 'detail': str(e) }, status=status.HTTP_400_BAD_REQUEST)

    @list_route(methods=['put'])
    def actualizar(self, request):
        "Actualiza un item de colaborador"
        try:
            colaborador_id = request.data.get('id', None)
            nombre = request.data.get('nombre', '')
            correo = request.data.get('correo', '')
            celular = request.data.get('celular', '')

            if colaborador_id:
                colaborador = Perfil.objects.get(pk=colaborador_id)
                persona = colaborador.persona
                persona.persona = nombre
                persona.correo = correo
                persona.celular = celular
                persona.save()
                return Response({ "status": status.HTTP_200_OK }, status=status.HTTP_200_OK)

            else:
                return Response({ 'detail': "No fué enviado el id del colaborador." }, status=status.HTTP_400_BAD_REQUEST)

        except Exception as e:
            print e
            return Response({ 'detail': str(e) }, status=status.HTTP_400_BAD_REQUEST)

    @list_route(methods=['post'], permission_classes=[AnyPermission])
    def cambiar_foto(self, request):
        "Endpoint para cambiar de foto en aplicación móvil"
        try:
            perfil_id = request.data.get('id', None)
            foto = request.data.get('foto', None)

            if not perfil_id:
                raise Exception("No se envió el ID del perfil")

            if foto:
                perfil = Perfil.objects.get(pk=perfil_id)
                persona = perfil.persona

                ##  HACER LA FOTO CUADRADA
                ##########
                size = (300, 300)
                image = Image.open(foto)
                image.thumbnail(size, Image.ANTIALIAS)
                background = Image.new('RGBA', size, (255, 255, 255, 0))
                background.paste(
                    image, (int((size[0] - image.size[0]) / 2), int((size[1] - image.size[1]) / 2))
                )
                ##########

                ##  A PARTIR DE LA IMAGEN PROCESADA, GENERAR UN BUFFER Y LUEGO UN ARCHIVO EN MEMORIA
                buffer1= StringIO()
                background.save(buffer1, format='PNG')
                img = InMemoryUploadedFile(buffer1, None,"{}.png".format(persona.id), 'image/png', buffer1.len, None)

                ##  GUARDAR EN BD LA NUEVA IMAGEN
                persona.foto = File(img)
                persona.save()

            return Response({ "status": status.HTTP_200_OK }, status=status.HTTP_200_OK)

        except Exception as e:
            print e
            return Response({ 'detail': str(e) }, status=status.HTTP_400_BAD_REQUEST)

    @list_route(methods=['put'])
    def restore(self, request):
        "Restore password for an user instance"
        try:
            ##  PARAMETROS
            perfil_id = request.data['id']
            password  = request.data['password']

            ##  ACTUALIZAR PASSWORD
            perfil = Perfil.objects.get(pk=perfil_id)
            user = perfil.user
            user.password = make_password(password)
            user.save()
            return Response({'detail': 'Contraseña restablecida exitosamente.'}, status=status.HTTP_200_OK)

        except Exception as e:
            return Response({ 'detail': str(e) }, status=status.HTTP_400_BAD_REQUEST)

    def destroy(self, request, *args, **kwargs):
        "User deactivator (Soft Delete)"
        try:
            perfil = self.get_object()

            ######################################################################
            ##  ELIMINAR A UN USUARIO DIFERENTE DE EL ACTUALMENTE LOGUEADO
            ######################################################################
            if self.request.user.id == perfil.user.id:
                raise Exception("No puedes eliminarte a ti mismo.")
            ######################################################################

            perfil.eliminado = True
            perfil.save()
            return Response(status=status.HTTP_204_NO_CONTENT)

        except Exception as e:
            print e
            return Response({ 'detail': str(e) }, status=status.HTTP_400_BAD_REQUEST)

    """
    @list_route(methods=['post'], permission_classes=[AllowAny])
    def restablecer(self, request):
        try:
            contrasena = request.data.get('contrasena')
            token = request.data.get('token')
            # Obtiene token
            tkc = TokenCorreo.objects.get(
                token=token,
                activo=True
            )
            t = timedelta(days=1)
            dt = t + tkc.fecha
            hoy = timezone.now()
            # Fecha no mayor a 1 dia valida
            if hoy <= dt:
                # Nueva contrasena
                usuario = User.objects.get(id=tkc.usuario_id)
                nueva = make_password(contrasena)
                usuario.password = nueva
                usuario.save()
                # Dar de baja al token
                tkc.activo = False
                tkc.save()
                return Response({'detail': 'OK'}, status=status.HTTP_200_OK)
            # Fecha mayor a un dia
            else:
                tkc.activo = False
                tkc.save()
                return Response({'detail': 'Fuera de fecha valida solicite un nuevo enlace'},
                                status=status.HTTP_400_BAD_REQUEST)

        except TokenCorreo.DoesNotExist:
            return Response({'detail': 'Enlace invalido, verifique si la direccion del enlace es la de su correo '
                                       'electronico o solicite un nuevo enlace'},
                            status=status.HTTP_400_BAD_REQUEST)
    """
