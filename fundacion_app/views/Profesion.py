# -*- coding: utf-8 -*-
from fundacion_app.models import Profesion
from rest_framework.response import Response
from rest_framework.decorators import list_route
from rest_framework import viewsets, status, filters
from fundacion_app.serializers import ProfesionSerializer

class ProfesionViewSet(viewsets.ModelViewSet):
    "Interfaz de programación de aplicaciónes para Profesiones"
    queryset = Profesion.objects.filter()
    #permission_classes = [AdminPermission]
    filter_backends = (filters.DjangoFilterBackend, filters.SearchFilter, filters.OrderingFilter)
    filter_fields = ('id', 'profesion')
    search_fields = ('id', 'profesion')
    ordering_fields = ('id', 'profesion')

    def get_queryset(self):
        "Query selector"
        return self.queryset.filter(eliminado=False).order_by('-fecha_creacion')

    def get_serializer_class(self):
        "Define serializer for API"
        return ProfesionSerializer

    def destroy(self, request, *args, **kwargs):
        "Profesion deactivator (Soft Delete)"
        try:
            profesion = self.get_object()
            profesion.eliminado = True
            profesion.save()
            return Response(status=status.HTTP_204_NO_CONTENT)

        except Exception as e:
            print e
            return Response({ 'detail': str(e) }, status=status.HTTP_400_BAD_REQUEST)
