# -*- coding: utf-8 -*-
from django.core.files import File
from rest_framework.response import Response
from rest_framework.decorators import list_route
from rest_framework import viewsets, status, filters
from fundacion_app.authorization import AnyPermission
from fundacion_app.utils.pdf_casos import pdf_cierre_caso
from fundacion_app.models import Caso, Perfil, NotaEvolutiva
from django.core.files.uploadedfile import InMemoryUploadedFile
from fundacion_app.serializers import CasoSerializer, CasoSerializerPopulate

import sys
reload(sys)
sys.setdefaultencoding('utf8')

class CasoViewSet(viewsets.ModelViewSet):
    "Interfaz de programación de aplicaciónes para Casos"
    queryset = Caso.objects.filter(eliminado=False).order_by('-fecha_creacion')
    filter_backends = (filters.DjangoFilterBackend, filters.SearchFilter, filters.OrderingFilter)
    filter_fields = ('id', 'colaborador', 'paciente', 'cerrado')
    search_fields = ('id', 'colaborador__persona__persona', 'paciente__persona__persona')
    ordering_fields = ('id', 'colaborador', 'paciente')

    def get_queryset(self):
        "Query selector"
        perfil = self.request.user.perfil
        if perfil:
            if Perfil.ADMIN in perfil.roles or Perfil.SECRETARIA in perfil.roles:
                # return self.queryset
                ##  SOLO EL PSICOLOGO PODRÁ VISUALIZAR SUS PROPIOS CASOS
                return self.queryset.filter(colaborador_id=perfil.id)
            else:
                return self.queryset.filter(colaborador_id=perfil.id)
        else:
            return self.queryset

    def get_serializer_class(self):
        "Define serializer for API"
        if self.action == "list" or self.action == "retrieve":
            return CasoSerializerPopulate
        else:
            return CasoSerializer

    @list_route(methods=['post'])
    def status(self, request):
        "Change status"
        try:
            id = request.data.get('id', None)
            caso = Caso.objects.get(pk=id)
            caso.cerrado = not caso.cerrado
            caso.save()

            serializer = CasoSerializer(caso, context={ 'request': request })
            return Response(serializer.data, status=status.HTTP_200_OK)
        except Exception as e:
            print e
            return Response({ "detail": str(e) }, status=status.HTTP_400_BAD_REQUEST)

    @list_route(methods=['post'])
    def genograma(self, request, *args, **kwargs):
        "Guardar la imagen del genograma de un caso"
        try:
            id = request.data.get('id', None)
            genograma = request.data.get('genograma', None)

            if id and type(genograma) is InMemoryUploadedFile:
                caso = Caso.objects.get(pk=id)
                caso.genograma = File(genograma)
                caso.save()

            return Response(status=status.HTTP_200_OK)

        except Exception as e:
            print e
            return Response({ 'detail': str(e) }, status=status.HTTP_400_BAD_REQUEST)

    @list_route(methods=['get'], permission_classes=[AnyPermission])
    def pdf_cierre_caso(self, request, *args, **kwargs):
        "Obtiene el PDF del cierre del caso"
        try:
            caso_id = request.GET.get('id', None)

            caso = Caso.objects \
                .filter(pk=caso_id) \
                .prefetch_related('colaborador__persona', 'paciente__persona') \
                .first()

            serializer = CasoSerializerPopulate(caso)
            caso_dict = serializer.data
            # caso_dict['notas'] = json.loads(caso_dict['notas'])

            notas_evolutivas = caso.notas_evolutivas.filter(anulada=False)

            ##  EL PDF DEVUELVE UN RESPONSE PARA RENDERIZARLO EN NAVEGADOR
            return pdf_cierre_caso(request, caso, caso_dict, notas_evolutivas)

        except Exception as e:
            print e
            return Response(str(e), status=status.HTTP_404_NOT_FOUND)

    def destroy(self, request, *args, **kwargs):
        "Caso soft delete"
        try:
            caso = self.get_object()
            caso.eliminado = True
            caso.save()
            return Response(status=status.HTTP_204_NO_CONTENT)

        except Exception as e:
            print e
            return Response({ 'detail': str(e) }, status=status.HTTP_400_BAD_REQUEST)
