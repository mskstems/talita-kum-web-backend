# -*- coding: utf-8 -*-
from django.utils import timezone
from rest_framework.response import Response
from rest_framework.decorators import list_route
from rest_framework import viewsets, status, filters
from fundacion_app.models import Cita, Perfil, Persona, Paciente
from fundacion_app.serializers import CitaSerializer, CitaSerializerPopulate

class CitaViewSet(viewsets.ModelViewSet):
    "Interfaz de programación de aplicaciónes para Citas"
    queryset = Cita.objects.all().order_by('-fecha_cita')
    filter_backends = (filters.DjangoFilterBackend, filters.SearchFilter, filters.OrderingFilter)
    FILTERS = (
        'id', 'paciente__persona__persona', 'colaborador__persona__persona'
    )
    filter_fields = ('id', 'colaborador', 'anulada')
    search_fields = FILTERS
    ordering_fields = FILTERS

    def get_queryset(self):
        "Query selector"
        perfil = self.request.user.perfil
        if perfil:
            if Perfil.ADMIN in perfil.roles or Perfil.SECRETARIA in perfil.roles:
                return self.queryset
            else:
                return self.queryset.filter(colaborador_id=perfil.id)
        else:
            return self.queryset

    def get_serializer_class(self):
        "Define serializer for API"
        if self.action == "list":
            return CitaSerializerPopulate
        elif self.action == "retrieve":
            return CitaSerializerPopulate
        else:
            return CitaSerializer

    @list_route(methods=['post'])
    def crear(self, request, *args, **kwargs):
        "API que devuelve todas las citas de la fecha de hoy en adelante"
        try:
            colaborador = request.data['colaborador']
            paciente_dict = request.data['paciente']
            descripcion = request.data['descripcion']
            fecha_cita = request.data['fecha_cita']

            print paciente_dict

            ##  SI EL PACIENTE NO TRAE ID, ES PORQUE HAY QUE CREAR UNO NUEVO
            paciente_id = paciente_dict.get('id', None)
            paciente = Paciente()
            if not paciente_id:
                persona = Persona.objects.create(
                    persona=paciente_dict.get('nombre', ''),
                    celular=paciente_dict.get('celular', '')
                )
                paciente = Paciente.objects.create(
                    persona=persona,
                    genero=paciente_dict.get('genero', '')
                )

            else:
                paciente = Paciente.objects.get(pk=paciente_id)

            ##  CREAR CITA
            cita = Cita.objects.create(
                colaborador_id = colaborador,
                paciente_id = paciente.id,
                descripcion = descripcion,
                fecha_cita = fecha_cita
            )

            serializer = CitaSerializerPopulate(cita, context={ "request": request })
            cita_json = serializer.data
            cita_json['status'] = status.HTTP_200_OK
            return Response(cita_json, status=status.HTTP_200_OK)

        except Exception as e:
            print e
            return Response({ 'detail': str(e) }, status=status.HTTP_400_BAD_REQUEST)


    @list_route(methods=['get'])
    def pendientes(self, request, *args, **kwargs):
        "API que devuelve todas las citas de la fecha de hoy en adelante"
        try:
            colaborador = request.GET.get('colaborador', None)
            anulada = request.GET.get('anulada', None)

            perfil = self.request.user.perfil

            if perfil:
                query =  Cita.objects.filter(fecha_cita__gte=timezone.now())

                # if Perfil.ADMIN in perfil.roles or Perfil.SECRETARIA in perfil.roles:
                #     ##  SI ES UN ADMIN O SECRETARIA ENVIA TODAS LAS CITAS DE TODOS LOS COLABORADORES
                #     pass

                if colaborador:
                    query = query.filter(colaborador_id=colaborador)

                if anulada == "true":
                    query = query.filter(anulada=True)
                elif anulada == "false":
                    query = query.filter(anulada=False)

                serializer = CitaSerializerPopulate(query, context={ "request": request }, many=True)

                results = { 'count': len(query), 'results': serializer.data }

                return Response(results, status=status.HTTP_200_OK)

            else:
                raise Exception("No tiene permiso para realizar esta petición")

        except Exception as e:
            print e
            return Response({ 'detail': str(e) }, status=status.HTTP_400_BAD_REQUEST)

    def destroy(self, request, *args, **kwargs):
        "Cita anulator"
        try:
            cita = self.get_object()
            cita.anulada = True
            cita.save()
            return Response(status=status.HTTP_204_NO_CONTENT)

        except Exception as e:
            print e
            return Response({ 'detail': str(e) }, status=status.HTTP_400_BAD_REQUEST)
