# -*- coding: utf-8 -*-
from fundacion_app.models import MovimientoCaja
from rest_framework.response import Response
from rest_framework.decorators import list_route
from rest_framework import viewsets, status, filters
from fundacion_app.serializers import MovimientoCajaSerializer, MovimientoCajaSerializerPopulate

class MovimientoCajaViewSet(viewsets.ModelViewSet):
    "Interfaz de programación de aplicaciónes para MovimientoCaja"
    queryset = MovimientoCaja.objects.filter()
    #permission_classes = [AdminPermission]
    filter_backends = (filters.DjangoFilterBackend, filters.SearchFilter, filters.OrderingFilter)
    filter_fields = ('id', 'apertura_cierre_caja')
    search_fields = ('id', 'concepto')
    ordering_fields = ('id', 'id')

    def get_queryset(self):
        "Query selector"
        return self.queryset.filter().order_by('-fecha_creacion')

    def get_serializer_class(self):
        "Define serializer for API"
        if self.action == "list":
            return MovimientoCajaSerializerPopulate
        elif self.action == "retrieve":
            return MovimientoCajaSerializerPopulate
        else:
            return MovimientoCajaSerializer

    def destroy(self, request, *args, **kwargs):
        "MovimientoCaja deactivator (Soft Delete)"
        try:
            mov = self.get_object()
            mov.anulado = True
            mov.save()
            return Response(status=status.HTTP_200_OK)

        except Exception as e:
            print e
            return Response({ 'detail': str(e) }, status=status.HTTP_400_BAD_REQUEST)
