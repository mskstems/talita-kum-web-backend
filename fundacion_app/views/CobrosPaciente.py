# -*- coding: utf-8 -*-
from rest_framework.response import Response
from rest_framework.decorators import list_route
from rest_framework import viewsets, status, filters
from fundacion_app.models import CobrosPaciente, Perfil
from fundacion_app.serializers import CobrosPacienteSerializer, CobrosPacienteSerializerPopulate

class CobrosPacienteViewSet(viewsets.ModelViewSet):
    "Interfaz de programación de aplicaciónes para CobrosPacientes"
    queryset = CobrosPaciente.objects.filter().order_by('-fecha_creacion')
    #permission_classes = [AdminPermission]
    filter_backends = (filters.DjangoFilterBackend, filters.SearchFilter, filters.OrderingFilter)
    filter_fields = ('id', 'paciente', 'pagado', 'anulado')
    search_fields = ('id', 'motivo', 'colaborador__persona__persona', 'paciente__persona__persona')
    ordering_fields = ('id', 'id')

    def get_queryset(self):
        "Query selector"
        perfil = self.request.user.perfil
        if perfil:
            if Perfil.ADMIN in perfil.roles or Perfil.SECRETARIA in perfil.roles:
                return self.queryset

            else:
                return self.queryset.filter(colaborador_id=perfil.id)
        else:
            return self.queryset

    def get_serializer_class(self):
        "Define serializer for API"
        # return CobrosPacienteSerializer
        if self.action == "list" or self.action == "retrieve":
            return CobrosPacienteSerializerPopulate
        else:
            return CobrosPacienteSerializer

    def destroy(self, request, *args, **kwargs):
        "CobrosPaciente anulator"
        try:
            cobro = self.get_object()

            if cobro.pagado:
                raise Exception("El cobro ya fue pagado. Por lo tanto, no puede ser anulado.")

            cobro.anulado = True
            cobro.save()
            return Response(status=status.HTTP_204_NO_CONTENT)

        except Exception as e:
            print e
            return Response({ 'detail': str(e) }, status=status.HTTP_400_BAD_REQUEST)
