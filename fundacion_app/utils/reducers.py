# -*- coding: utf-8 -*-
from django.core.files import File
from fundacion_app.models import Profesion
from django.core.files.uploadedfile import InMemoryUploadedFile

def guardar_perfil(perfil, context={}):
    "Guarda la información del perfil en base al contexto"
    roles = [ str(rol) for rol in context.get('roles', perfil.roles) ]
    perfil.roles        = roles
    perfil.colegiado    = context.get('colegiado', perfil.colegiado)

    profesion = context.get('profesion', None)
    if not profesion:
        profesion = None

    perfil.profesion_id = profesion

    perfil.save()
    return perfil

def guardar_persona(persona, context={}):
    "Guarda la información de la persona en base a la información del contexto"
    foto = context.get('foto', None)
    if type(foto) is InMemoryUploadedFile:
        persona.foto = File(context['foto'])

    persona.cui             = context.get('cui', persona.cui)
    persona.correo          = context.get('username', persona.correo)
    persona.celular         = context.get('celular', persona.celular)
    persona.persona         = context.get('persona', persona.persona)
    persona.telefono        = context.get('telefono', persona.telefono)
    persona.direccion       = context.get('direccion', persona.direccion)
    persona.nacionalidad    = context.get('nacionalidad', persona.nacionalidad)
    persona.departamento    = context.get('departamento', persona.departamento)
    persona.apellido_casada = context.get('apellido_casada', persona.apellido_casada)
    persona.save()
    return persona

"""
def guardar_paciente(paciente, context={}):
    tipos = [ str(tipo) for tipo in context.get('tipos', paciente.tipos) ]
    paciente.tipos               = tipos
    paciente.genero              = context.get('genero', paciente.genero)
    paciente.persona_id          = context.get('persona')
    paciente.religion            = context.get('religion', paciente.religion)
    paciente.ocupacion           = context.get('ocupacion', paciente.ocupacion)
    paciente.num_hermanos        = context.get('num_hermanos', paciente.num_hermanos)
    paciente.tipo_de_sangre      = context.get('tipo_de_sangre', paciente.tipo_de_sangre)
    paciente.nivel_academico     = context.get('nivel_academico', paciente.nivel_academico)
    paciente.lugar_nacimiento    = context.get('lugar_nacimiento', paciente.lugar_nacimiento)
    paciente.fecha_nacimiento    = context.get('fecha_nacimiento', paciente.fecha_nacimiento)
    paciente.contacto_referencia = context.get('contacto_referencia', paciente.contacto_referencia)
    paciente.telefono_referencia = context.get('telefono_referencia', paciente.telefono_referencia)
    paciente.save()
    return paciente
"""
