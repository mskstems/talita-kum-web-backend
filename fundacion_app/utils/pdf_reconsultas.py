# -*- coding: utf-8 -*-
import json
from datetime import datetime
from django.http import HttpResponse
from fundacion_app.models import Reconsulta
from django.contrib.sites.models import Site
from fundacion_app.utils.utils import diff_month
from wkhtmltopdf.views import PDFTemplateResponse

def pdf_tratamiento(request, reconsulta_dict):
    "Genera el PDF del tratamiento - receta del médico"

    now = datetime.now().strftime("%d/%m/%Y")

    ##  RENDER PDF
    template= '../templates/pdf/tratamiento.reconsulta.html'
    current_site = Site.objects.get_current()

    ##  LOGO IMAGE
    img_logo = str(current_site) + "/static/fundacion/logo_transparente.png"

    #   CONTEXTO DE DATOS PARA LA PLANTILLA DEL PDF
    context= {
        'logo': img_logo,
        'reconsulta': reconsulta_dict,
        'now': now
    }

    #   CONFIGURACION DE LA HOJA DEL PDF
    cmd_options= {
        'page-size': 'A6',
        'margin-top': 28,
        'margin-right': 8,
        'margin-left': 8,
        'margin-bottom':24,
        'encoding': "UTF-8",
        'no-outline': None,
        'orientation': 'portrait'
    }

    ##  RETORNA LA VISTA PDF
    # request = RequestFactory().get('/')
    response = PDFTemplateResponse(
        request=request,
        header_template="../templates/pdf/tratamiento.header.html",
        template=template,
        filename="tratamiento_reconsulta.pdf",
        context=context,
        show_content_in_browser=True,
        cmd_options=cmd_options
    )

    ##  GUARDAR EL ARCHIVO PDF EN UN BUFFER CON STRINGIO E INMEMORYUPLOADEDFILE PARA PODER GUARDARLO EN BD
    # _buffer = response.rendered_content
    # _stringio = StringIO(_buffer)
    # _memory_file = InMemoryUploadedFile(_stringio, None, "carnet.pdf", 'application/pdf', _stringio.len, None)
    # return _memory_file

    ##  RETORNA EL ARCHIVO PDF
    # response = HttpResponse(response.rendered_content, content_type='application/pdf')
    # response['Content-Disposition'] = 'attachment; filename="tratamiento.pdf"'

    #   RETORNA LA VISTA HTML
    #response = render(request, template, context)

    return response


def pdf_expediente(request, reconsulta, reconsulta_dict):
    "Genera el PDF de consultas - receta del médico"

    now = datetime.now().strftime("%d/%m/%Y")

    reconsulta_dict['fecha_creacion'] = reconsulta.fecha_creacion

    ##  CALCULAR EDAD
    if reconsulta.consulta.paciente.fecha_nacimiento:
        try:
            reconsulta_dict['consulta']['paciente']['edad'] = "{} años".format(
                diff_month(reconsulta.fecha_creacion, reconsulta.consulta.paciente.fecha_nacimiento)
            )
        except:
            reconsulta_dict['consulta']['paciente']['edad'] = "---"
    else:
        reconsulta_dict['consulta']['paciente']['edad'] = "---"

    ##  CALCULAR NUMERO DE EXPEDIENTE
    num_reconsulta = Reconsulta.objects \
        .filter(fecha_creacion__lt=reconsulta.fecha_creacion, eliminado=False, consulta_id=reconsulta.consulta_id) \
        .count() + 1


    ##  RENDER PDF
    template= '../templates/pdf/reconsultas.html'
    current_site = Site.objects.get_current()

    ##  LOGO IMAGE
    img_logo = str(current_site) + "/static/fundacion/logo_transparente.png"

    #   CONTEXTO DE DATOS PARA LA PLANTILLA DEL PDF
    context= {
        'logo': img_logo,
        'reconsulta': reconsulta_dict,
        'now': now,
        'num_reconsulta': num_reconsulta
    }

    #   CONFIGURACION DE LA HOJA DEL PDF
    cmd_options= {
        'page-size': 'Letter',
        'margin-top': 38,
        'margin-right': 16,
        'margin-left': 16,
        'margin-bottom': 20,
        'encoding': "UTF-8",
        'no-outline': None,
        'orientation': 'portrait'
    }

    ##  RETORNA LA VISTA PDF
    response = PDFTemplateResponse(
        request=request,
        header_template="../templates/pdf/consultas.header.html",
        footer_template="../templates/pdf/pagination.footer.html",
        template=template,
        filename="Reconsulta No. {} del expediente No. {}.pdf".format(num_reconsulta, reconsulta.id),
        context=context,
        show_content_in_browser=True,
        cmd_options=cmd_options,
    )

    return response
