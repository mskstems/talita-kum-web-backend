# -*- coding: utf-8 -*-
from datetime import datetime
from django.contrib.sites.models import Site
from fundacion_app.models import MovimientoCaja
from wkhtmltopdf.views import PDFTemplateResponse
from fundacion_app.utils.utils import formato_quetzal

def pdf_reporte_movimientos_caja(request, info, movimientos_caja_bd, movimientos):
    "Reporte de movimientos de caja entres fechas en formato PDF"
    now = datetime.now().strftime("%d/%m/%Y")

    # apertura_cierre_dict['fecha_creacion'] = apertura_cierre.fecha_creacion
    # apertura_cierre_dict['fecha_cierre'] = apertura_cierre.fecha_cierre

    # apertura_cierre_dict['total_apertura'] = formato_quetzal(apertura_cierre_dict['total_apertura'])
    # apertura_cierre_dict['total_cierre'] = formato_quetzal(apertura_cierre_dict['total_cierre'])

    # apertura_cierre_dict['ingresos_egresos']['ingresos'] = formato_quetzal(apertura_cierre_dict['ingresos_egresos']['ingresos'])
    # apertura_cierre_dict['ingresos_egresos']['egresos'] = formato_quetzal(apertura_cierre_dict['ingresos_egresos']['egresos'])

    # movs = apertura_cierre_dict.get('movimientos', [])
    total_ingresos = 0
    total_egresos = 0
    index = 0
    for mov in movimientos:

        # mov['fecha_creacion'] = movimientos_caja_bd[index].fecha_creacion
        mov['fecha_creacion'] = movimientos_caja_bd[index].fecha_creacion.strftime("%d-%m-%Y %H:%M")

        if mov['tipo'] == MovimientoCaja.INGRESO:
            total_ingresos += mov['cantidad']
        elif mov['tipo'] == MovimientoCaja.EGRESO:
            total_egresos += mov['cantidad']
        mov['cantidad'] = formato_quetzal(mov['cantidad'])

        index += 1

    info["total_ingresos"] = formato_quetzal(total_ingresos)
    info["total_egresos"] = formato_quetzal(total_egresos)

    ##  RENDER PDF
    template= '../templates/pdf/reporte.movimientos.caja.html'
    current_site = Site.objects.get_current()

    ##  LOGO IMAGE
    img_logo = str(current_site) + "/static/fundacion/logo_transparente.png"

    #   CONTEXTO DE DATOS PARA LA PLANTILLA DEL PDF
    context= {
        'logo': img_logo,
        'info': info,
        'movimientos': movimientos,
        'now': now
    }

    #   CONFIGURACION DE LA HOJA DEL PDF
    cmd_options= {
        'page-size': 'Letter',
        'margin-top': 38,
        'margin-right': 16,
        'margin-left': 16,
        'margin-bottom': 20,
        'encoding': "UTF-8",
        'no-outline': None,
        'orientation': 'portrait'
    }

    ##  RETORNA LA VISTA PDF
    response = PDFTemplateResponse(
        request=request,
        header_template="../templates/pdf/format_a4.header.html",
        footer_template="../templates/pdf/pagination.footer.html",
        template=template,
        filename="Reporte de movimientos de caja del {} al {}.pdf".format(info['fecha_inicial'], info['fecha_final']),
        context=context,
        show_content_in_browser=True,
        cmd_options=cmd_options,
    )

    return response


def pdf_cierre_caja(request, apertura_cierre, apertura_cierre_dict):
    "Genera el PDF de cierre de caja"

    now = datetime.now().strftime("%d/%m/%Y")

    apertura_cierre_dict['fecha_creacion'] = apertura_cierre.fecha_creacion
    apertura_cierre_dict['fecha_cierre'] = apertura_cierre.fecha_cierre

    #apertura_cierre_dict['total_apertura'] = "Q " + ("%.2f" % apertura_cierre_dict["total_apertura"])
    #apertura_cierre_dict['total_cierre'] = "Q " + ("%.2f" % apertura_cierre_dict["total_cierre"])
    apertura_cierre_dict['total_apertura'] = formato_quetzal(apertura_cierre_dict['total_apertura'])
    apertura_cierre_dict['total_cierre'] = formato_quetzal(apertura_cierre_dict['total_cierre'])

    #apertura_cierre_dict['ingresos_egresos']['ingresos'] = "Q " + ("%.2f" % apertura_cierre_dict["ingresos_egresos"]['ingresos'])
    #apertura_cierre_dict['ingresos_egresos']['egresos'] = "Q " + ("%.2f" % apertura_cierre_dict["ingresos_egresos"]['egresos'])
    apertura_cierre_dict['ingresos_egresos']['ingresos'] = formato_quetzal(apertura_cierre_dict['ingresos_egresos']['ingresos'])
    apertura_cierre_dict['ingresos_egresos']['egresos'] = formato_quetzal(apertura_cierre_dict['ingresos_egresos']['egresos'])

    ##  RENDER PDF
    template= '../templates/pdf/cierre_caja.html'
    current_site = Site.objects.get_current()

    ##  LOGO IMAGE
    img_logo = str(current_site) + "/static/fundacion/logo_transparente.png"

    #   CONTEXTO DE DATOS PARA LA PLANTILLA DEL PDF
    context= {
        'logo': img_logo,
        'apertura_cierre': apertura_cierre_dict,
        'now': now
    }

    #   CONFIGURACION DE LA HOJA DEL PDF
    cmd_options= {
        'page-size': 'A6',
        'margin-top': 24,
        'margin-right': 8,
        'margin-left': 8,
        'margin-bottom':24,
        'encoding': "UTF-8",
        'no-outline': None,
        'orientation': 'portrait'
    }

    ##  RETORNA LA VISTA PDF
    # request = RequestFactory().get('/')
    response = PDFTemplateResponse(
        request=request,
        header_template="../templates/pdf/format_a6.header.html",
        template=template,
        filename="Cierre de caja No. {}.pdf".format(apertura_cierre.id),
        context=context,
        show_content_in_browser=True,
        cmd_options=cmd_options,
    )

    return response

def pdf_estado_cuenta_caja(request, apertura_cierre, apertura_cierre_dict):
    "Estado de cuenta de caja en formato PDF"
    now = datetime.now().strftime("%d/%m/%Y")

    apertura_cierre_dict['fecha_creacion'] = apertura_cierre.fecha_creacion
    apertura_cierre_dict['fecha_cierre'] = apertura_cierre.fecha_cierre

    apertura_cierre_dict['total_apertura'] = formato_quetzal(apertura_cierre_dict['total_apertura'])
    apertura_cierre_dict['total_cierre'] = formato_quetzal(apertura_cierre_dict['total_cierre'])

    apertura_cierre_dict['ingresos_egresos']['ingresos'] = formato_quetzal(apertura_cierre_dict['ingresos_egresos']['ingresos'])
    apertura_cierre_dict['ingresos_egresos']['egresos'] = formato_quetzal(apertura_cierre_dict['ingresos_egresos']['egresos'])

    movs = apertura_cierre_dict.get('movimientos', [])
    for mov in movs:
        mov['cantidad'] = formato_quetzal(mov['cantidad'])

    ##  RENDER PDF
    template= '../templates/pdf/estado_cuenta_caja.html'
    current_site = Site.objects.get_current()

    ##  LOGO IMAGE
    img_logo = str(current_site) + "/static/fundacion/logo_transparente.png"

    #   CONTEXTO DE DATOS PARA LA PLANTILLA DEL PDF
    context= {
        'logo': img_logo,
        'apertura_cierre': apertura_cierre_dict,
        'now': now
    }

    #   CONFIGURACION DE LA HOJA DEL PDF
    cmd_options= {
        'page-size': 'Letter',
        'margin-top': 38,
        'margin-right': 16,
        'margin-left': 16,
        'margin-bottom': 20,
        'encoding': "UTF-8",
        'no-outline': None,
        'orientation': 'portrait'
    }

    ##  RETORNA LA VISTA PDF
    response = PDFTemplateResponse(
        request=request,
        header_template="../templates/pdf/format_a4.header.html",
        footer_template="../templates/pdf/pagination.footer.html",
        template=template,
        filename="Estado de cuenta del cierre de caja No. {}.pdf".format(apertura_cierre.id),
        context=context,
        show_content_in_browser=True,
        cmd_options=cmd_options,
    )

    return response
