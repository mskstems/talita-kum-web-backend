# -*- coding: utf-8 -*-
import json
from datetime import datetime
from django.http import HttpResponse
from django.contrib.sites.models import Site
from fundacion_app.utils.utils import diff_month
from wkhtmltopdf.views import PDFTemplateResponse

def pdf_cierre_caso(request, caso, caso_dict, notas_evolutivas):
    "Genera el PDF de cierre de caso"

    now = datetime.now().strftime("%d/%m/%Y")

    caso_dict['fecha_creacion'] = caso.fecha_creacion

    if (caso_dict['fecha_cierre']):
        caso_dict['fecha_cierre'] = caso.fecha_cierre

    if caso.paciente.fecha_nacimiento:
        try:
            caso_dict['paciente']['edad'] = "{} años".format(
                diff_month(caso.fecha_creacion, caso.paciente.fecha_nacimiento)
            )
        except:
            caso_dict['paciente']['edad'] = "---"
    else:
        caso_dict['paciente']['edad'] = "---"

    ##  RENDER PDF
    template= '../templates/pdf/cierre_caso.html'
    current_site = Site.objects.get_current()

    ##  LOGO IMAGE
    img_logo = str(current_site) + "/static/fundacion/logo_transparente.png"

    #   CONTEXTO DE DATOS PARA LA PLANTILLA DEL PDF
    context= {
        'logo': img_logo,
        'caso': caso_dict,
        'now': now,
        'sesiones': notas_evolutivas.count()
    }

    #   CONFIGURACION DE LA HOJA DEL PDF
    cmd_options= {
        'page-size': 'Letter',
        'margin-top': 38,
        'margin-right': 16,
        'margin-left': 16,
        'margin-bottom': 20,
        'encoding': "UTF-8",
        'no-outline': None,
        'orientation': 'portrait'
    }

    ##  RETORNA LA VISTA PDF
    # request = RequestFactory().get('/')
    response = PDFTemplateResponse(
        request=request,
        header_template="../templates/pdf/psicologia.header.html",
        footer_template="../templates/pdf/pagination.footer.html",
        template=template,
        filename="Cierre de caso No. {}.pdf".format(caso.id),
        context=context,
        show_content_in_browser=True,
        cmd_options=cmd_options,
    )

    return response

