# -*- coding: utf-8 -*-
import json
from datetime import datetime
from django.http import HttpResponse
from django.contrib.sites.models import Site
from fundacion_app.utils.utils import diff_month
from wkhtmltopdf.views import PDFTemplateResponse

def pdf_tratamiento(request, consulta_dict):
    "Genera el PDF del tratamiento - receta del médico"

    now = datetime.now().strftime("%d/%m/%Y")

    ##  RENDER PDF
    template= '../templates/pdf/tratamiento.html'
    current_site = Site.objects.get_current()

    ##  LOGO IMAGE
    img_logo = str(current_site) + "/static/fundacion/logo_transparente.png"

    #   CONTEXTO DE DATOS PARA LA PLANTILLA DEL PDF
    context= {
        'logo': img_logo,
        'consulta': consulta_dict,
        'now': now
    }

    #   CONFIGURACION DE LA HOJA DEL PDF
    cmd_options= {
        'page-size': 'A6',
        'margin-top': 28,
        'margin-right': 8,
        'margin-left': 8,
        'margin-bottom':24,
        'encoding': "UTF-8",
        'no-outline': None,
        'orientation': 'portrait'
    }

    ##  RETORNA LA VISTA PDF
    # request = RequestFactory().get('/')
    response = PDFTemplateResponse(
        request=request,
        header_template="../templates/pdf/tratamiento.header.html",
        template=template,
        filename="tratamiento.pdf",
        context=context,
        show_content_in_browser=True,
        cmd_options=cmd_options
    )

    ##  GUARDAR EL ARCHIVO PDF EN UN BUFFER CON STRINGIO E INMEMORYUPLOADEDFILE PARA PODER GUARDARLO EN BD
    # _buffer = response.rendered_content
    # _stringio = StringIO(_buffer)
    # _memory_file = InMemoryUploadedFile(_stringio, None, "carnet.pdf", 'application/pdf', _stringio.len, None)
    # return _memory_file

    ##  RETORNA EL ARCHIVO PDF
    # response = HttpResponse(response.rendered_content, content_type='application/pdf')
    # response['Content-Disposition'] = 'attachment; filename="tratamiento.pdf"'

    #   RETORNA LA VISTA HTML
    #response = render(request, template, context)

    return response


def pdf_expediente(request, consulta, consulta_dict, reconsultas, reconsultas_dict):
    "Genera el PDF de consultas - receta del médico"

    now = datetime.now().strftime("%d/%m/%Y")

    consulta_dict['fecha_creacion'] = consulta.fecha_creacion

    index = 0
    for rec_dict in reconsultas_dict:
        rec_dict['fecha_creacion'] = reconsultas[index].fecha_creacion
        index += 1

    if consulta.paciente.fecha_nacimiento:
        try:
            consulta_dict['paciente']['edad'] = "{} años".format(
                diff_month(consulta.fecha_creacion, consulta.paciente.fecha_nacimiento)
            )
        except:
            consulta_dict['paciente']['edad'] = "---"
    else:
        consulta_dict['paciente']['edad'] = "---"

    ##  RENDER PDF
    template= '../templates/pdf/consultas.html'
    current_site = Site.objects.get_current()

    ##  LOGO IMAGE
    img_logo = str(current_site) + "/static/fundacion/logo_transparente.png"

    #   CONTEXTO DE DATOS PARA LA PLANTILLA DEL PDF
    context= {
        'logo': img_logo,
        'consulta': consulta_dict,
        'reconsultas': reconsultas_dict,
        'now': now
    }

    #   CONFIGURACION DE LA HOJA DEL PDF
    cmd_options= {
        'page-size': 'Letter',
        'margin-top': 38,
        'margin-right': 16,
        'margin-left': 16,
        'margin-bottom': 20,
        'encoding': "UTF-8",
        'no-outline': None,
        'orientation': 'portrait'
    }

    ##  RETORNA LA VISTA PDF
    # request = RequestFactory().get('/')
    response = PDFTemplateResponse(
        request=request,
        header_template="../templates/pdf/consultas.header.html",
        footer_template="../templates/pdf/pagination.footer.html",
        template=template,
        filename="Expediente No. {}.pdf".format(consulta.id),
        context=context,
        show_content_in_browser=True,
        cmd_options=cmd_options,
    )

    return response
