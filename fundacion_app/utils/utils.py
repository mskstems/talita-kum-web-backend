# -*- coding: utf-8 -*-
import math
from PIL import Image
from django.db.models import Sum
from fundacion_app.models import MovimientoCaja

def make_square(im, min_size=256, fill_color=(0, 0, 0, 0)):
    x, y = im.size
    size = max(min_size, x, y)
    new_im = Image.new('RGBA', (size, size), fill_color)
    new_im.paste(im, ((size - x) / 2, (size - y) / 2))
    return new_im

def diff_month(end_date, start_date):
    "Devuelve la diferencia en años entre dos fechas"
    # res1 = (end_date.year - start_date.year) * 12 + end_date.month - start_date.month
    # res2 = ((end_date.month - start_date.month) + ((end_date.year - start_date.year) * 12)) + 1
    return end_date.year - start_date.year - ((end_date.month, end_date.day) < (start_date.month, start_date.day))

def formato_quetzal(cantidad):
    "Retorna un número convertido a string con el formato del Quetzal ya sea positivo o negativo"
    try:
        cantidad_float = float(cantidad)
        cantidad_abs = math.fabs(cantidad_float)
        cantidad_formateada = "Q " + ("%.2f" % cantidad_abs)

        ##  SI EL VALOR FLOAT ES MENOR A 0, EN VEZ DE LLEVAR EL GUÍON (-)
        ##  VA ENTRE PARÉNTESIS EL VALOR NEGATIVO COMO MEDIDA MONETARIA NEGATIVA
        if cantidad_float < 0:
            cantidad_formateada = "({})".format(cantidad_formateada)

        return cantidad_formateada

    except Exception as e:
        print e
        return str(cantidad)

def determinar_ingresos_egresos(apertura_cierre_id):
    "Devuelve el total de ingresos y egresos de cada apertura_cierre de caja"
    try:
        movimientos_ingresos = MovimientoCaja.objects \
            .filter(apertura_cierre_caja_id=apertura_cierre_id, tipo=MovimientoCaja.INGRESO) \
            .aggregate(Sum('cantidad'))

        movimientos_egresos = MovimientoCaja.objects \
            .filter(apertura_cierre_caja_id=apertura_cierre_id, tipo=MovimientoCaja.EGRESO) \
            .aggregate(Sum('cantidad'))

        ##  OBTENER SOLAMENTE EL VALOR SUMA DE LOS INGRESOS Y EGRESOS
        total_ingresos = movimientos_ingresos.get('cantidad__sum', 0)
        total_egresos = movimientos_egresos.get('cantidad__sum', 0)

        ##  EN CASO DE HABER DEVUELTO UN NONE SE SETEARÁ A 0 EL VALOR PARA AMBOS CASOS
        total_ingresos = total_ingresos if total_ingresos else 0
        total_egresos = total_egresos if total_egresos else 0

        return {
            "ingresos": total_ingresos,
            "egresos": total_egresos
        }

    except Exception as e:
        print e
        return {
            "ingresos": 0,
            "egresos": 0
        }
