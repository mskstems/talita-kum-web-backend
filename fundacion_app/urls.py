"""fundacion URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
# -*- coding: utf-8 -*-
from fundacion_app import views
from rest_framework import routers
from django.conf.urls import url, include
from rest_framework.authtoken.views import obtain_auth_token

router = routers.DefaultRouter()

router.register(r'perfiles', views.PerfilViewSet)
router.register(r'profesiones', views.ProfesionViewSet)
router.register(r'pacientes', views.PacienteViewSet)
router.register(r'citas', views.CitaViewSet)
router.register(r'casos', views.CasoViewSet)
router.register(r'notas_evolutivas', views.NotaEvolutivaViewSet)
router.register(r'consultas', views.ConsultaViewSet)
router.register(r'reconsultas', views.ReconsultaViewSet)
router.register(r'archivos_consulta', views.ArchivoConsultaViewSet)
router.register(r'caja', views.CajaViewSet)
router.register(r'aperturas_cierres_caja', views.AperturaCierreCajaViewSet)
router.register(r'movimientos_caja', views.MovimientoCajaViewSet)
router.register(r'cobros_paciente', views.CobrosPacienteViewSet)

urlpatterns = [
    url(r'^api/', include(router.urls)),
    url(r'^api/login', obtain_auth_token, name='api-login'),
    url(r'^api-auth', include('rest_framework.urls', namespace='rest_framework')),
]
