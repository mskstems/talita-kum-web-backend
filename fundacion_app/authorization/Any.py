from rest_framework import permissions

class AnyPermission(permissions.BasePermission):
    """
    Global permission check for all users.
    """

    def has_permission(self, request, view):
        return True
