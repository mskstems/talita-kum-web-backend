# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import os, requests, django
###########################################################
os.environ['DJANGO_SETTINGS_MODULE'] = 'fundacion.settings'
django.setup()
###########################################################
from django.conf import settings
from fundacion_app.models import TipoCambio

def get_exchange_rate():
    "Get the latest USD --> GTQ exchange rate"
    try:
        available_currencies = [ "GTQ" ]
        params = {
            u"currencies": u", ".join(available_currencies),
            u"access_key": str(settings.ACCESS_KEY_EXCHANGERATE),
            u"format": "1"
        }

        response = requests.get(settings.URL_API_EXCHANGERATE, params=params)
        if response.status_code == 200:
            ##  SI RETORNÓ UN 200, PROCESÓ LA PETICIÓN CON ÉXITO
            quotes = response.json().get('quotes', {})
            usd_to_gtq = quotes.get('USDGTQ', None)

            if usd_to_gtq:
                ##  GUARDAR EN BD EL TIPO DE CAMBIO ACTUAL
                tipocambio = TipoCambio.objects.first()
                if tipocambio:
                    ##  SI SE ENCUENTRA EN BD ACTUALIZAR EL VALOR
                    tipocambio.usd_to_gtq = float(usd_to_gtq)
                    tipocambio.save()

                else:
                    ##  SI NO ESTÁ EN BD, CREAR EL ITEM DE TIPOCAMBIO
                    tipocambio = TipoCambio.objects.create(
                        usd_to_gtq=float(usd_to_gtq)
                    )

            else:
                ##  SI NO SE ENCONTRÓ EL TIPO DE CAMBIO LEVANTA UNA EXCEPCIÓN CON EL RESPONSE CONTENT
                raise Exception(response.content)

            return True

        else:
            ##  SI RETORNÓ UN CÓDIGO NO IGUAL A 200 IMPRIME EL RESPONSE EN LA CONSOLA
            raise Exception(response.content)

    except Exception as e:
        print e
        return False

if __name__ == "__main__":
    get_exchange_rate()
