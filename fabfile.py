from fabric.api import env, cd, run, local, prefix

##  SERVER
#env.app_dir = '~/www/app'
#env.hosts = ['app@ip']
env.key_filename = '~/.ssh/id_rsa'
env.git_branch = 'master'
env.build_frontend = True
env.virtualenv = 'fundacion'


def deploy():
    with cd(env.app_dir):
        run('git reset --hard')
        run('git checkout {}'.format(env.git_branch))
        run('git pull')

        # frontend
        if env.build_frontend:
            with cd('Frontend'):
                run('npm install')
                run('bower install')
                run('gulp build')
        local('bash build.sh')

        # backend
        with prefix('source /etc/bash_completion.d/virtualenvwrapper'):
            with prefix('workon {}'.format(env.virtualenv)):
                run('pip install --upgrade --no-cache-dir -r requirements.txt')
                run('python manage.py migrate')
                run('python manage.py collectstatic --noinput')
                run('touch caritas/local_settings.py')

        run('sudo supervisorctl restart all')